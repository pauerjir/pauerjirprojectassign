/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE DATABASE passignpr1_2013;

GRANT ALL PRIVILEGES ON passignpr1_2013.* to 'project-assign' WITH GRANT OPTION;

USE passignpr1_2013;

--
-- Table structure for table `Hodnoceni`
--

DROP TABLE IF EXISTS `Hodnoceni`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hodnoceni` (
  `id_Hodnoceni` int(11) NOT NULL AUTO_INCREMENT,
  `hodnoceni` text,
  `id_User` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Hodnoceni`),
  KEY `FK29D0A13B63784A0B` (`id_User`),
  CONSTRAINT `Hodnoceni_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Paralelka`
--

DROP TABLE IF EXISTS `Paralelka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Paralelka` (
  `id_Paralelka` int(11) NOT NULL AUTO_INCREMENT,
  `cas` int(11) DEFAULT NULL,
  `cisloParalelky` int(11) DEFAULT NULL,
  `deadline_vyber` datetime DEFAULT NULL,
  `den` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Paralelka`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Semestralka`
--

DROP TABLE IF EXISTS `Semestralka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Semestralka` (
  `id_Zadani` int(11) DEFAULT NULL,
  `id_User` int(11) NOT NULL,
  PRIMARY KEY (`id_User`),
  KEY `FK53EC85A875A1CE73` (`id_Zadani`),
  KEY `FK53EC85A863784A0B` (`id_User`),
  CONSTRAINT `Semestralka_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `Semestralka_ibfk_2` FOREIGN KEY (`id_Zadani`) REFERENCES `Zadani` (`id_Zadani`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Soubory`
--

DROP TABLE IF EXISTS `Soubory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Soubory` (
  `id_Soubor` int(11) NOT NULL AUTO_INCREMENT,
  `cesta` varchar(255) DEFAULT NULL,
  `datumNahrani` datetime DEFAULT NULL,
  `datumStazeni` datetime DEFAULT NULL,
  `puvodniJmeno` varchar(255) DEFAULT NULL,
  `stahnuto` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_Soubor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Technologie`
--

DROP TABLE IF EXISTS `Technologie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Technologie` (
  `id_Technologie` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_Technologie`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TechnologieZadani`
--

DROP TABLE IF EXISTS `TechnologieZadani`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TechnologieZadani` (
  `id_Zadani` int(11) NOT NULL,
  `id_Technologie` int(11) NOT NULL,
  KEY `FK47B7C6C875A1CE73` (`id_Zadani`),
  KEY `FK47B7C6C87FAD05BD` (`id_Technologie`),
  CONSTRAINT `TechnologieZadani_ibfk_3` FOREIGN KEY (`id_Technologie`) REFERENCES `Technologie` (`id_Technologie`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `TechnologieZadani_ibfk_4` FOREIGN KEY (`id_Zadani`) REFERENCES `Zadani` (`id_Zadani`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `User`
--

DROP TABLE IF EXISTS `User`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `User` (
  `id_User` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `surname` varchar(255) DEFAULT NULL,
  `userName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_User`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserParalelka`
--

DROP TABLE IF EXISTS `UserParalelka`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserParalelka` (
  `id_Paralelka` int(11) NOT NULL DEFAULT '0',
  `id_User` int(11) NOT NULL,
  PRIMARY KEY (`id_Paralelka`,`id_User`),
  KEY `FK191FF71E63784A0B` (`id_User`),
  KEY `FK191FF71E2C34CC3D` (`id_Paralelka`),
  CONSTRAINT `UserParalelka_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `UserParalelka_ibfk_2` FOREIGN KEY (`id_Paralelka`) REFERENCES `Paralelka` (`id_Paralelka`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UserRole`
--

DROP TABLE IF EXISTS `UserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRole` (
  `id_UserRole` int(11) NOT NULL AUTO_INCREMENT,
  `id_Role` int(11) DEFAULT NULL,
  `id_User` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_UserRole`),
  KEY `FKF3F7670163784A0B` (`id_User`),
  CONSTRAINT `UserRole_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Zadani`
--

DROP TABLE IF EXISTS `Zadani`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Zadani` (
  `id_Zadani` int(11) NOT NULL AUTO_INCREMENT,
  `nazev` varchar(100) DEFAULT NULL,
  `popis` text,
  `stav` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_Zadani`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userSoubor`
--

DROP TABLE IF EXISTS `userSoubor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userSoubor` (
  `id_User` int(11) DEFAULT NULL,
  `id_Soubor` int(11) NOT NULL,
  PRIMARY KEY (`id_Soubor`),
  KEY `FK5EA0C1375F58044D` (`id_Soubor`),
  KEY `FK5EA0C13763784A0B` (`id_User`),
  CONSTRAINT `userSoubor_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `User` (`id_User`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `userSoubor_ibfk_2` FOREIGN KEY (`id_Soubor`) REFERENCES `Soubory` (`id_Soubor`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `User`
--

LOCK TABLES `User` WRITE;
/*!40000 ALTER TABLE `User` DISABLE KEYS */;
INSERT INTO `User` VALUES (1,'balikm1@fel.cvut.cz','Balík','6e017b5464f820a6c1bb5e9f6d711a667a80d8ea','Martin ','balikm1');
/*!40000 ALTER TABLE `User` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `UserRole`
--

LOCK TABLES `UserRole` WRITE;
/*!40000 ALTER TABLE `UserRole` DISABLE KEYS */;
INSERT INTO `UserRole` VALUES (1,2,1);
/*!40000 ALTER TABLE `UserRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Zadani`
--

LOCK TABLES `Zadani` WRITE;
/*!40000 ALTER TABLE `Zadani` DISABLE KEYS */;
INSERT INTO `Zadani` VALUES (1,'Ruleta','Sázení dle pravidel rulety. V každém kole generování náhodného čísla. Evidence účtu uživatele včetně osobních údajů a historie sázení. V průběhu ukládání stavu do binárního souboru. Možnost exportu \"účtenky\" - informaci o stavu konta do textového souboru.',1),(2,'Racionální kalkulačka','Program načte výraz, který může obsahovat celá čísla a operace sčítání, odčítání, násobení, dělení a mocnění a závorky, a provede jeho vyhodnocení s plnou přesností (výsledkem bude celé číslo nebo zlomek), s ohledem na prioritu operátorů a závorky; výraz je možné zadávat přímo programu nebo načítat / ukládat z / do souboru, stačí konzolové uživatelské rozhraní.',1),(3,'Maticové výpočty','Program by měl umět:\r\n-vytvořit libovolne velkou matici\r\n-sčítat, odčítat, násobit matice\r\n-spočítat determinant čtvercové matice (do 4. řádu)\r\n-výpočet vlastních čísel matice\r\n-spočítat hodnost matice\r\n-transpozice matice\r\n-inverzní matice (do 4.řádu)\r\n-provést Gaussovu eliminační metodu\r\n-řešení soustavy lineárních rovnic\r\n\r\nU programu bude umožněn vstup a výstup do textoveho souboru, kde vstupní soubor zajišťuje přísun dat a příkazů ve stejné formě jako by byly zadávány ručně / interaktivní vstup bude také možný. Výstupní soubor slouží k ukládání výsledků (popř. ukládání postupu výpočtu).\r\n\r\n',1),(4,'Lodě','Implementujte program modelující populární hru \"Lodě\" v režimu \"člověk vs. počítač\".\r\n\r\nStručný popis hry :     \r\nHra pro dva hráče. Na začatku každý má k dispozici hrací plochu - pole 10 x 10, jehož jednotlivé sloupce jsou očíslované zprava doleva a řádky shora dolů. Před začátkem boje každý hráč musí rozmístit na svém pole  lodě. Počet a charakteristika lodí : 4 jednopalubné ( * ), 3 dvoupalubné( ** ), 2  trojpalubné ( *** ) a jedna čtýřpalubná ( **** ). Lodě lze rozmístit vertikálně či vodorovně, bez žádných zlomů. Lodě se nesmí dotýkat ani rohem, ani bokem, stejně jako se nesmí překrývat.  Po ukončení tohoto kroku začíná boj. Pravidla : hráč zadává souřadnice prvku soupeřova pole, kam chce střílet. Soupeř mu pak odpoví, zda zasahl nějakou loď. Varianty odpovědi : a) zasáhl = tady byla jen část celé lodě; b) potopil = spolu s předchozímí tahy jste potopil celou loď; c) mimo. Pak následuje tah soupeře a tak se střídají, dokud někdo nezníčí celou soupeřovu flotilu <=> vyhraje.\r\n\r\nHru bude možné v průběhu uložit a pozdeji znovu načíst.\r\nPo dokončení hry se každý zápas zapíše do textového souboru v podobě jmen hráčů a kdo byl vítěz. Tuto statistiku si bude moci uživatel prohlédnout i v samotném programu.\r\n\r\n',1),(5,'Logik','Hra pro jednoho hráče proti počítači, podle pravidel:\r\nhttp://cs.wikipedia.org/wiki/Logik_%28hra%29\r\nMožnost uložení a načtení rozehrané hry.\r\n\r\n',1),(6,'Had','Grafická hra had pro jednoho či více hráčů na jednom počítači.\r\nPočítání skóre, nejlepší výsledky, ukládání výsledků do souboru.\r\n(Nutné samostudium grafické knihovny Swing)\r\n\r\n',1),(7,'Sudoku','Program načte ze souboru zadání / rozehranou hru, uživatel může zadat tah, u kterého program ověří jeho přípustnost (vzhledem k pravidlům); program může navrhnout tah vedoucí k řešení, umožní hru uložit\r\n\r\n',1),(8,'Hledání min','Pravidla stejná jako Minesweeper. Možnost výběru z několika velikostí hracího pole a hustoty \"min\". Možnost minu odpálit nebo označit vlaječkou. Zaznamenávání statistik a rekordních časů v jednotlivých kategoriích.\r\n\r\n',1),(9,'Tréninkový deník','Databáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\nHlavní funkce aplikace:\r\n\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\nexport výsledku hledání do přehledného textového souboru\r\n\r\n',1),(10,'Hra GO','Hra pro dva hráče na jednom počítači, program zobrazí hrací pole s obsazenými pozicemi (rozlišitelnými podle hráče), v každém tahu hráč na výzvu zadá pozici, kterou chce obsadit (např. kartézskými souřadnicemi); program kontroluje dodržování pravidel (http://cs.wikipedia.org/wiki/Go) (nedovolí nepřípustné tahy) a podle nich také provádí automaticky některé akce; možnost uložení a načtení rozehrané hry\r\n\r\n',1),(11,'Textová adventura','Textová adventura: Hra se bude odehrávat v budově o několika přístupných místnostech. K dispozici budou různé předměty ukryté v místnostech, které se budou muset vhodně posbírat a zkombinovat pro splnění cíle (útěku). Hru bude mozne uložit (pokračovat druhý den), při nahrání se však uložená hra odstraní, takže nebude možné se při chybě vrátit.\r\n\r\n',1),(12,'Reversi','Hra pro dva hráče na jednom počítači, program zobrazí hrací pole s obsazenými pozicemi (rozlišitelnými podle hráče), v každém tahu hráč na výzvu zadá pozici, kterou chce obsadit (např. kartézskými souřadnicemi); program kontroluje dodržování pravidel (http://en.wikipedia.org/wiki/Reversi) - nedovolí nepřípustné tahy; podle pravidel také provádí automaticky některé akce; možnost uložení a načtení rozehrané hry. Také možnost hry proti počítači, který bude hrát náhodnými tahy nebo jednoduchou umělou inteligencí.\r\n\r\n',1),(13,'Šifrování','Program bude umet kódovat/dekódovat Caesarovu sifru\r\nhttp://cs.wikipedia.org/wiki/Caesarova_%C5%A1ifra\r\nVigenerovu sifru\r\nhttp://cs.wikipedia.org/wiki/Vigen%C3%A8rova_%C5%A1ifra\r\na Morseovu abecedu.\r\nVstup bude mozny uzivatelem z klavesnice nebo ze souboru.\r\nVystup do souboru nebo na konzoli.\r\n\r\n',1),(14,'Automat na prodej jízdenek','Automat by měl umět samostatně vypočítávat ceny jízdenek, kupující zadá odkud kam bude chtít jet. Automat vypíše vzdálenost a cenu jízdného. Kupující poté zadá hodnotu, kterou bude platit jízdné. Pokud zadá malou hodnotu, automat mu nic nevydá, při vyšší částce automat automaticky vypočítá kolik mu má vrátit a druhy mincí. Správce bude moci zadávat další města a vzdálenosti jízdenek a i jejich cenu, existující záznamy bude možné vyhledávat a editovat.\r\n\r\n',1),(15,'Kniha jízd','Databáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\ndata typu: počet km, trasa, důvod cesty, průměrná spotřeba, datum atd.\r\n\r\nHlavní funkce aplikace:\r\n\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\nexport výsledku hledání do přehledného textového souboru\r\n\r\n',1),(16,'Šibenice','Hádání slov pomocí písmen. Bude možnost zvolit různé obtížnosti, podle délky slov od 5 do 12, uživatel bude zadávat písmena a pokud zadá písmeno, které je obsaženo v hádaném slově, vypíše se na příslušném místě/místech ve slově. Dále se bude pomocí znaků vykreslovat jak moc jste „oběšený“, bude možno přidávat, odebírat, vypisovat a řadit slova která jsou v programu obsažena a umožňovat vyhledání slova podle jeho části. Program bude také evidovat úspěšnost hráče a umět zobrazit jeho statistiku.\r\n\r\n',1),(17,'Knihovna','Evidence knih (název knihy, autor, země a rok vydání, žánr, vydavatelství...), čtenářů(Jméno, přímnení, rodné číslo...) a výpůjček. Každé knihy je určitý počet. V případě nedostupnosti si čtenář může udělat rezervaci.\r\nAdministrační rozhraní pro správu knih a uživatelů a uživatelské rozhraní pro výpůjčku.\r\n\r\nDatabáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\nHlavní funkce aplikace:\r\n\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\nexport vyhledaných či vypůjčených knih do textového souboru (čitelné pro tisk).\r\n\r\n',1),(18,'člověče, nezlob se','Hra pro 2 až 4 hráče na jednom počítači (hráče možno nahradit počítačem(automatický hráč), nebo pozici nechat volnou), program zobrazí hrací pole s aktuálním rozestavěním figurek (musí být rozlišitelné podle hráčů), po hodu kostkou (vygenerované náhodné číslo) si hráč zvolí figurku, se kterou táhnout; program kontroluje dodržování pravidel a nabízí pouze přípustné tahy; možnost uložení a načtení rozehrané hry\r\npravidla: http://cs.wikipedia.org/wiki/%C4%8Clov%C4%9B%C4%8De_nezlob_se\r\nHerní plán může být zobrazen např. takto:\r\n|0|.0.0.0.0.0.0.0.0.0.|0|.0.0.0.0.0.0.0.0.0.|0|.0.0.0.0.0.0.0.0.0.|0|.0.0.0.0.0.0.0.0.0.\r\n\r\n',1),(19,'Dáma','Hra pro dva hráče na jednom počítači. Program zobrazí hrací pole s aktuálním rozmístěním rozehrané (nebo nové) hry, v každém kole příslušný hráč vhodně volí následující tah. Program umožňuje načíst a uložit rozehranou hru z nebo do souboru, kontroluje přípustnost tahů vzhledem k pravidlům.\r\nhttp://cs.wikipedia.org/wiki/D%C3%A1ma_%28hra%29\r\n\r\n',1),(20,'Piškvorky','Hra pro dva hráče na jednom počítači, program zobrazí hrací pole s aktuálně obsazenými pozicemi (rozlišitelné podle hráčů), v každém tahu hráč na výzvu zadává pozici, kterou chce obsadit (např. kartézskými souřadnicemi); program kontroluje, zda požadovaná pozice není obsazená a zda nějaký hráč vyhrál (pravidla - http://cs.wikipedia.org/wiki/Pi%C5%A1kvorky); velikost hracího pole se zadává před začátkem hry; možnost uložení a načtení rozehrané hry\r\n\r\n',1),(21,'Zkoušení ze slovíček','Program načte ze souboru dvojice slovíček (např. v češtině a angličtině) a vyzkouší uživatele ze zadaného počtu z nich, ke zkoušení budou slovíčka vybírána v náhodném pořádí, avšak s ohledem k úspěšnosti uživatele, po zkoušení se zobrazí statistika úspěšnosti zkoušeného; dvojice slovíček bude možno v programu zadávat i mazat.\r\n\r\n',1),(22,'Židi','Hra pro dva hráče na jednom počítači, program zobrazí hrací pole s obsazenými pozicemi (rozlišitelnými podle hráče), v každém tahu hráč na výzvu zadá pozici, kterou chce obsadit (např. kartézskými souřadnicemi); program kontroluje, zda požadovaná pozice není obsazená a dále, zda nějaký hráč vyhrál (podle pravidel - http://cs.wikipedia.org/wiki/%C5%BDidi) velikost hracího pole může být pevně daná (dostatečná); možnost uložení a načtení rozehrané hry.\r\n\r\n',1),(23,'Trubky','Hra pro dva hráče na jednom počítači, hráč na výzvu zadává pozici, kterou chce obsadit; program kontroluje dodržování pravidel (http://cs.wikipedia.org/wiki/Trubky_%28hra%29), umožní uložení rozehrané hry a její opětovné načtení\r\n\r\n',1),(24,'Evidence CD/DVD','data typu: název, autor, typ média, jednotlivé položky (skladby, filmy)\r\n\r\nDatabáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\nHlavní funkce aplikace:\r\n\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\nexport výsledku hledání do přehledného textového souboru\r\n\r\n',1),(25,'Šachy','Hra pro dva hráče na jednom počítači. Program zobrazí hrací pole s aktuálním rozmístěním rozehrané (nebo nové) hry, v každém kole příslušný hráč vhodně volí následující tah. Program umožňuje načíst a uložit rozehranou hru z nebo do souboru, kontroluje přípustnost tahů vzhledem k pravidlům, včetně kontroly šachu a matu (http://cs.wikipedia.org/wiki/%C5%A0achy).\r\n\r\n',1),(26,'Autobazar','Program bude evidovat seznam automobilů, kde ke každému budou uložené informace (výrobce, model, stav tachometru, STK, rok výroby, druh karoserie, počet majitelů, výbava, barva, cena).\r\nAuta bude možné v systému rezervovat zákazníkovi, nebo provést záznam o koupi.\r\n\r\nDatabáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\nHlavní funkce aplikace:\r\n\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\nexport výsledku hledání do přehledného textového souboru\r\n\r\n',1),(27,'Game of Life','Program bude simulovat \"Game of Life\" (http://en.wikipedia.org/wiki/Conway%27s_Game_of_Life) v poli o konstantních rozměrech. Bude podporovat krokování i plynulý vývoj generací. Bude schopen načítat a ukládat \"herní\" pole do souboru.\r\n\r\n',1),(28,'Prší','Hra pro dva hráče na jednom počítači.\r\nProgram bude zobrazovat balíček vyhozených karet pomocí slovního pojmenování. V kazdém kole hráč zadá jakou kartu chce hodit do balíčku. Program kontroluje správnost tahu. Pokud nebude možné provézt tento tah nedovolí ho.\r\nNa začátku hry se hráči identifikují jménem, program bude evidovat počty jejich výher a proher, výpis statistiky, statistiku bude možné vyexportovat do textového souboru. Stav hry bude možné uložit do binárního souboru a znovu načíst.\r\n\r\n',1),(29,'Převaděč dat z SQL skriptu','Program bude sloužit ke generování dat z SQL skriptu - rozšířený SQL insert\r\nINSERT INTO table (id, event) VALUES (1, 94263), (2, 75015), (3, 75015);\r\nMožné vstupy - konzole, soubor.\r\nBude možné zadat více insertů. Program již sám zjistí, zda se jedná o novou tabulku či existující. Program ověří správnost syntaxe příkazu.\r\nMožné výstupy: CSV, XML, HTML\r\nCSV: dle textového konfiguračního souboru programu určí oddělovač, zda bude součástí záhlaví, název souboru pojmenuje podle tabulky.\r\nXML: bude obsahovat data všech insertů ve vhodné struktuře - tabulky, sloupce, data\r\nHTML: podobně jako XML, ale tabulky budou čitelně formatovány v HTML stránce, kterou bude možné zobrazit v prohlížeči.\r\nProgram bude mít dva módy\r\n1) konzolový - vstup ze souboru a veškeré nastavení určené parametry programu z příkazové řádky\r\n2) interaktivní - uživatel bude programem dotazován k zadávání údajů.\r\n\r\n',1),(30,'Evidence obchodu s elektronikou','- Evidence zboží (název, popis, cena, počet kusů atd.)\r\n- kategorie zboží\r\n- nákupní košík\r\n\r\nDatabáze reprezentovaná pomocí datové struktury a objektů s možností uložení do souboru, nejedná se o SQL databázi!\r\n\r\nHlavní funkce aplikace:\r\n\r\n',1),(31,'Nástroj pro tvorbu testů','Program sloužící k tvorbě testů a následnému zkoušení.\r\nPodle parametrů (počet otázek, obtížnost či zaměření) bude umět náhodně vygenerovat test z dostupných otázek.\r\nZkoušený bude vybírat z nabízených možností a po dokončení mu bude test vyhodnocen (počty bodů, známka). Report výsledku testu bude možné uložit do textového souboru.\r\n\r\nV administračním rozhraní bude správa množiny otázek.\r\nZáznam otázky může obsahovat informace např. text otázky, obtížnost či zaměření, nabízené možnosti odpovědí a správné řešení. Budou podporovány také odpovědi typu žádná správně nebo více správně.\r\nRozhraní bude podporovat:\r\nvkládání záznamů\r\nmazání záznamů\r\neditace záznamů\r\nvýpis a seřazení záznamů podle kritérií\r\nvyhledávání záznamů\r\nuložení/načtení binárního souboru\r\n\r\n',1),(32,'Pexeso Plus','Netradiční varianta hry Pexeso.\r\nKromě klasické verze hry bude obsahovat i těžší variantu s následující úpravou pravidel:\r\n - při otočení dvou odlišných karet si tyto dvě karty nejdříve vymění místo a poté se otočí zpět rubem vzhůru (jde o náročnější verzi hry pro dospělé a zkušené hráče pexesa).\r\n\r\nHra proti počítači nebo proti jinému hráči na stejném počítači.\r\nMožnost nastavení vlastností hry (velikost hracího pole, výběr sady karet, obtížnost počítačového protivníka). Nastavení bude uloženo v binárním souboru.\r\nDo textového souboru se bude ukládat tabulka nejlepších hráčů.',1);
/*!40000 ALTER TABLE `Zadani` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

SET GLOBAL binlog_format = 'ROW';

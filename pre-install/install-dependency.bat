call mvn install:install-file -Dfile=ASF.Core-0.1.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-core -Dversion=0.1.RELEASE -Dpackaging=jar
call mvn install:install-file -Dfile=ASF.Persistence.Hibernate-0.1.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-persistence-hibernate -Dversion=0.1.RELEASE -Dpackaging=jar
call mvn install:install-file -Dfile=ASF.Web.Primefaces-0.1.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-web-primefaces -Dversion=0.1.RELEASE -Dpackaging=jar

call mvn install:install-file -Dfile=ASF.Core-0.2.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-core -Dversion=0.2.RELEASE -Dpackaging=jar -Djavadoc=ASF.Core-0.2-javadoc.jar
call mvn install:install-file -Dfile=ASF.Persistence.Hibernate-0.2.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-persistence-hibernate -Dversion=0.2.RELEASE -Dpackaging=jar -Djavadoc=ASF.Persistence.Hibernate-0.2-javadoc.jar
call mvn install:install-file -Dfile=ASF.Web.Primefaces-0.2.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-web-primefaces -Dversion=0.2.RELEASE -Dpackaging=jar -Djavadoc=ASF.Web.Primefaces-0.2-javadoc.jar

call mvn install:install-file -Dfile=ASF.Core-0.3-SNAPSHOT.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-core -Dversion=0.3-SNAPSHOT -Dpackaging=jar -Djavadoc=ASF.Core-0.3-SNAPSHOT-javadoc.jar
call mvn install:install-file -Dfile=ASF.Persistence.Hibernate-0.3-SNAPSHOT.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-persistence-hibernate -Dversion=0.3-SNAPSHOT -Dpackaging=jar -Djavadoc=ASF.Persistence.Hibernate-0.3-SNAPSHOT-javadoc.jar
call mvn install:install-file -Dfile=ASF.Web.Primefaces-0.3-SNAPSHOT.jar -DgroupId=cz.cvut.fel.asf -DartifactId=asf-web-primefaces -Dversion=0.3-SNAPSHOT -Dpackaging=jar -Djavadoc=ASF.Web.Primefaces-0.3-SNAPSHOT-javadoc.jar
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.services.HodnoceniService;
import cz.cvut.fel.projectassign.services.UserService;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testovací třída třídy HodnoceniServiceImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class HodnoceniServiceImplTest {

    @Autowired
    HodnoceniService instance;
    @Autowired
    UserService userService;
    String komentar;
    User user;
    Hodnoceni hod;

    @Before
    public void setUp() {
        user = userService.create(komentar, komentar, komentar, komentar, komentar);
        hod = instance.create(komentar, user);
    }

    /**
     * Test of findById method, of class HodnoceniServiceImpl.
     */
    @Test
    public void testFindById() {

        Hodnoceni result = instance.findById(hod.getId());
        assertEquals(hod, result);

    }

    /**
     * Test of update method, of class HodnoceniServiceImpl.
     */
    @Test
    public void testUpdate() {
        String nove = "nove";
        hod.setHodnoceni(nove);
        assertEquals(nove, hod.getHodnoceni());
    }
}

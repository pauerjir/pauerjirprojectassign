/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *Testovací třída třídy ParalelkaServiceImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class ParalelkaServiceImplTest {

    @Autowired
    ParalelkaService instance;
    Paralelka par;
    int cislo = 1011;

    @Before
    public void setUp() {
        par = instance.create(cislo, cislo, cislo);
    }

    /**
     * Test of create method, of class ParalelkaServiceImpl.
     */
    /**
     * Test of getByClassNumber method, of class ParalelkaServiceImpl.
     */
    @Test
    public void testGetByClassNumber() {

        Paralelka result = instance.getByClassNumber(cislo);
        assertEquals(par, result);
    }

    /**
     * Test of update method, of class ParalelkaServiceImpl.
     */
//    @Test
//    public void testUpdate() {
//
//        int nove = 11;
//        par.setCisloParalelky(nove);
//        instance.update(par);
//        assertEquals(par.getCisloParalelky(), nove);
//    }

    /**
     * Test of getAllParalelka method, of class ParalelkaServiceImpl.
     */
    @Test
    public void testGetAllParalelka() {

        List result = instance.getAllParalelka();
        assertTrue(result.contains(par));

    }

    /**
     * Test of findById method, of class ParalelkaServiceImpl.
     */
    @Test
    public void testFindById() {
        Paralelka result = instance.findById(par.getId());
        assertEquals(par, result);
    }

    /**
     * Test of getDayString method, of class ParalelkaServiceImpl.
     */
    @Test
    public void testGetDayString() {

        String result = instance.getDayString(par);
        assertEquals("Chybný den", result);

    }

    /**
     * Test of getCasString method, of class ParalelkaServiceImpl.
     */
    @Test
    public void testGetCasString() {
        String result = instance.getCasString(par);
        assertEquals("Chybný čas", result);
    }
}

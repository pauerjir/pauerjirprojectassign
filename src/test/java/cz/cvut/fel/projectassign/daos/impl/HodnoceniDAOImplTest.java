/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.daos.HodnoceniDAO;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Testovací třída třídy HodnoceniDAOImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class HodnoceniDAOImplTest {

    @Autowired
    HodnoceniDAO hodnoceniDao;
    @Autowired
    UserDAOImpl userDao;
    Hodnoceni hodnoceni;
    String komentar = "hruza";
    User user;

    @Before
    public void setUp() {
        user = userDao.create(komentar, komentar, komentar, komentar, komentar);
    }

    /**
     * Test of create method, of class HodnoceniDAOImpl.
     */
    @Test
    public void testCreate() {
        hodnoceni = hodnoceniDao.create(komentar, user);
        assertEquals(hodnoceni.getHodnoceni(), komentar);

    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.daos.ParalelkaDAO;
import cz.cvut.fel.projectassign.daos.UserDAO;
import cz.cvut.fel.projectassign.daos.UserRoleDAO;
import cz.cvut.fel.projectassign.daos.ZadaniDAO;
import cz.cvut.fel.projectassign.model.*;
import org.hibernate.SessionFactory;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
/**
 * Testovací třída třídy ParalelkaDAOImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class ParalelkaDAOImplTest {

    @Autowired
    ParalelkaDAO paralelkaDao;
    @Autowired
    UserDAO userDao;
    @Autowired
    ZadaniDAO zadaniDao;
    @Autowired
    UserRoleDAO userRoleDao;
    
    int cislo = 100;
    Paralelka paralelka;

    @Before
    public void before() {
        paralelka = paralelkaDao.create(cislo);
    }

    /**
     * Test of findByNumber method, of class ParalelkaDAOImpl.
     */
    @Test
    public void testFindByNumber() {
        Paralelka result = paralelkaDao.findByNumber(cislo);
        assertEquals(paralelka, result);

    }
    
    @Test
    public void testFindByRequestedZadaniId() {
        User user = userDao.create("novakj", "pass", "email@test.cz", "Jan", "Novak");
        userRoleDao.create(user, UserRole.USER);
        user.addParalelka(paralelka);
        Zadani zadani = zadaniDao.create("Novak's zadani", "popis", ZadaniStatus.REQUESTED);
        user.setZadani(zadani);
        
        Paralelka par2 = paralelkaDao.create(102);
        User user2 = userDao.create("cechp", "pass", "cech@test.cz", "Petr", "Čech");
        userRoleDao.create(user2, UserRole.USER);
        user2.addParalelka(par2);
        Zadani zadani2 = zadaniDao.create("Cech's zadani", "popis", ZadaniStatus.PUBLIC);
        user2.setZadani(zadani2);
        
        Paralelka result = paralelkaDao.findByRequestedZadaniId(zadani.getId());
        assertEquals(paralelka, result);
        
        Paralelka result2 = paralelkaDao.findByRequestedZadaniId(zadani2.getId());
        assertNull(result2);
    }
}

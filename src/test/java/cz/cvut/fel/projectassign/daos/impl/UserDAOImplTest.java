/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.daos.UserRoleDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *Testovací třída třídy UserDAOImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class UserDAOImplTest {

    @Autowired
    UserDAOImpl userDao;
    
    @Autowired
    UserRoleDAO userRoleDao;
    
    @Autowired
    ParalelkaDAOImpl paralelkaDao;
    
    int cislo = 100;
    User user;
    Paralelka paralelka;
    String name = "nick";

    @Before
    public void before() {
        paralelka = paralelkaDao.create(5);
        user = userDao.create(name, "heslo", "mail", "jmeno", "prijmeni");
        user.addParalelka(paralelka);
        //userDao.save(user);
        userRoleDao.create(user, UserRole.TUTOR);
    }

    /**
     * Test of findByName method, of class UserDAOImpl.
     */
    @Test
    public void testFindByName() {
        User result = userDao.findByName(name);
        assertEquals("chyba", name, result.getUserName());

    }

    /**
     * Test of findByLogin method, of class UserDAOImpl.
     */
    @Test
    public void testFindByLogin() {
        User result = userDao.findByName(name);
        assertEquals("chyba", name, result.getUserName());
        assertTrue("findByLogin: user role failed", result.hasRole(UserRole.TUTOR));
    }

    public void testFindAllByName() {
        List<User> result = userDao.findAllByName(name);
        assertEquals("chyba", name, result.get(0).getJmeno());
    }
    
    @Test
    public void testfindTutorByParalelka() {
        User result = userDao.findTutorByParalelka(paralelka);
        assertEquals("chyba findtutorByParalelka", user, result);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.model.Soubor;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *Testovací třída třídy SouborDAOImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class SouborDAOImplTest {

    @Autowired
    SouborDAOImpl instance;
    String cesta = "\\home\\test";
    Soubor soubor;

    @Before
    public void setUp() {
        soubor = instance.create(cesta, "puvodni_jmeno");
    }

    /**
     * Test of findByCesta method, of class SouborDAOImpl.
     */
    @Test
    public void testFindByCesta() {

        Soubor result = instance.findByCesta(cesta);
        assertEquals(soubor, result);
    }
}

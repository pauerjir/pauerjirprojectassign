/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.Zadani;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *Testovací třída třídy ZadaniDaoImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class ZadaniDaoImplTest {

    @Autowired
    ZadaniDaoImpl instance;
    @Autowired
    UserDAOImpl daoUser;
    @Autowired
    ParalelkaDAOImpl daoPar;
    Zadani zadani;
    User user, user2;
    Paralelka paralelka2, paralelka;
    String nazev = "nazev";

    @Before
    public void before() {
        zadani = instance.create(nazev, "popis", 1);

        user = daoUser.create("username", "heslo", "email", "jmeno", "prijmeni");
        user2 = daoUser.create("username2", "heslo2", "email2", "jmeno2", "prijmeni2");
        paralelka = new Paralelka(100);
        paralelka2 = new Paralelka(100);
        user.setZadani(zadani);
        user.addParalelka(paralelka);
        user2.addParalelka(paralelka);
        user.setZadani(zadani);
        //daoPar.save(paralelka);
        //instance.save(zadani);
        //daoUser.save(user);
        //daoUser.save(user2);
    }

    /**
     * Test of findByStatus method, of class ZadaniDaoImpl.
     */
    @Test
    public void testFindByStatus() {

        List<Zadani> result = instance.findByStatus(1);
        assertEquals("not same", nazev, result.get(0).getNazev());
    }

    /**
     * Test of findByName method, of class ZadaniDaoImpl.
     */
    @Test
    public void testFindByName() {

        Zadani result = instance.findByName(nazev);
        assertEquals(nazev, result.getNazev());

    }

    /**
     * Test of findIdenticalZadaniByParalelka method, of class ZadaniDaoImpl.
     */
    @Test
    public void testFindZadaniByParalelka() {
        zadani.getId();
        Zadani result = instance.findIdenticalZadaniByParalelka(zadani, (Paralelka)user2.getParalelka().toArray()[0]);
        assertEquals(null, result);

    }

    /**
     * Test of findAllByNameStav method, of class ZadaniDaoImpl.
     */
    @Test
    public void testFindAllByNameStav() {
        List<Zadani> result = instance.findAllByNameStav("naz", "4");
        assertEquals("not same", nazev, result.get(0).getNazev());


    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.projectassign.model.Technologie;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *Testovací třída třídy TechnologieDaoImpl
 * @author Tom
 */
@Transactional
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/applicationContextTest.xml")
public class TechnologieDaoImplTest {

    @Autowired
    TechnologieDaoImpl instance;
    Technologie tech;
    String nazev = "SQL";

    @Before
    public void setUp() {
        tech = instance.create(nazev);


    }

    /**
     * Test of findByName method, of class TechnologieDaoImpl.
     */
    @Test
    public void testFindByName() {

        Technologie result = instance.findByName(nazev);
        assertEquals(tech, result);

    }

    /**
     * Test of findAllByName method, of class TechnologieDaoImpl.
     */
    @Test
    public void testFindAllByName() {

        List result = instance.findAllByName(nazev);
        assertTrue(result.contains(tech));

    }
}

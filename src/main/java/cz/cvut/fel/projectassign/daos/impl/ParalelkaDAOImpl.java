/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.ParalelkaDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.ZadaniStatus;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databází tykající se paralelek
 * @author Tom
 */
@Repository
public class ParalelkaDAOImpl extends AbstractDAO<Paralelka, Integer> implements ParalelkaDAO, ICanDelete<Paralelka> {

    @Override
    public Paralelka create(int cislo, int cas, int den) {
        checkCanCreate(cislo).throwEx();
        
        Paralelka paralelka = new Paralelka(cislo, cas, den);
        save(paralelka);
        return paralelka;
    }

    @Override
    public Paralelka create(int cislo) {
        checkCanCreate(cislo).throwEx();
        
        Paralelka paralelka = new Paralelka(cislo);
        save(paralelka);
        return paralelka;
    }

    @Override
    public Paralelka findByNumber(int cislo) {
        return (Paralelka) currentSession().createCriteria(Paralelka.class).add(Restrictions.eq("cisloParalelky", cislo)).setMaxResults(1).uniqueResult();
    }
    
    @Override
    public Paralelka findByRequestedZadaniId(int zadaniId) {
        String hqlExpression = String.format("select p"
                + " from Zadani z"
                + " join z.zadaniStudents zs"
                + " join zs.paralelka p"
                + " where z.stav = %d"
                + " and z.id = :id", ZadaniStatus.REQUESTED);

        List<Paralelka> result = (List<Paralelka>) currentSession().createQuery(hqlExpression)
                .setInteger("id", zadaniId)
                .list();

        if (result.size() > 1)
            throw new RuntimeException("findByRequestedZadaniId: Multiple results found!");
        
        return result.isEmpty() ? null : (Paralelka)result.get(0);
    }

    private BusinessCheckResult checkCanCreate(int par) {
        return new BusinessCheckSimpleResult(findByNumber(par) == null);
    }

    @Override
    public BusinessCheckResult checkCanDelete(Paralelka par) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(Paralelka p) {
        if (!currentSession().contains(p))
            currentSession().update(p);
    }
}

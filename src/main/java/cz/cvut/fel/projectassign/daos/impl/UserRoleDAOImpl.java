/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.UserRoleDAO;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databázítykající se userRole
 * @author Tom
 */
@Repository
public class UserRoleDAOImpl extends AbstractDAO<UserRole, Integer> implements UserRoleDAO, ICanDelete<UserRole> {

    @Override
    public UserRole create(User user, int role) {
        checkCanCreate().throwEx();
        
        UserRole ur = new UserRole(role, user);
        save(ur);
        return ur;
    }

    private BusinessCheckResult checkCanCreate() {
        return new BusinessCheckSimpleResult(true);
    }

    @Override
    public BusinessCheckResult checkCanDelete(UserRole ur) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(UserRole ur) {
        if (!currentSession().contains(ur))
            currentSession().update(ur);
    }
}

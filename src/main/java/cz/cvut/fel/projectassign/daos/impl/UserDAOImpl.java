/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanComplexDelete;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.UserDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávajícíperzistenční operace nad databází tykající se uživatele
 * @author Tom
 */
@Repository
public class UserDAOImpl extends AbstractDAO<User, Integer> implements UserDAO, ICanDelete<User> {

    @Override
    public User create(String userName, String heslo, String email, String jmeno, String prijmeni) {
        checkCanCreate(userName).throwEx();
                
        User user = new User(userName, heslo, email, jmeno, prijmeni);
        save(user);
        return user;
    }

    //<editor-fold defaultstate="collapsed" desc="Find methods">
    @Override
    public User findByName(String name) {
        User output = (User) currentSession().createCriteria(User.class).add(Restrictions.eq("userName", name)).uniqueResult();
        return output;
    }

    @Override
    public User findByLogin(String loginName) {
        User output = (User) currentSession().createCriteria(User.class).add(Restrictions.eq("userName", loginName)).uniqueResult();
        return output;
    }

    @Override
    public List<User> findAllByName(String search) {
        return currentSession().createCriteria(User.class).add(Restrictions.ilike("userName", "%" + search + "%")).list();
    }
    
    @Override
    public User findTutorByParalelka(Paralelka par) {
        String hqlExpression = String.format("select u"
                + " from User u"
                + " join u.userRoles ur"
                + " join u.paralelka p"
                + " where ur.idRole = %s"
                + "  and p = :paralelka", UserRole.TUTOR);

        List<User> result = (List<User>) currentSession().createQuery(hqlExpression)
                .setEntity("paralelka", par)
                .list();

        if (result.size() > 1)
            throw new RuntimeException("findTutorByParalelka: Multiple results found!");
        
        return result.isEmpty() ? null : (User)result.get(0);
    }
    //</editor-fold>

    private BusinessCheckResult checkCanCreate(String username) {
        return new BusinessCheckSimpleResult(findByName(username) == null, "User %s already exists", username);
    }

    @Override
    public BusinessCheckResult checkCanDelete(User user) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(User u) {
        if (!currentSession().contains(u))
            currentSession().update(u);
    }
}

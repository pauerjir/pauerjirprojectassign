/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.TechnologieDAO;
import cz.cvut.fel.projectassign.model.Technologie;
import java.util.List;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databází tykající se technologii
 * @author Tom
 */
@Repository
public class TechnologieDaoImpl extends AbstractDAO<Technologie, Integer> implements TechnologieDAO, ICanDelete<Technologie> {

    @Override
    public Technologie create(String nazev) {
        checkCanCreate(nazev).throwEx();
        
        Technologie technologie = new Technologie(nazev);
        save(technologie);
        return technologie;
    }

    @Override
    public Technologie findByName(String name) {
        Technologie output = (Technologie) currentSession().createCriteria(Technologie.class).add(Restrictions.eq("nazev", name)).uniqueResult();
        return output;
    }

    @Override
    public List<Technologie> findAllByName(String nazev) {
        return currentSession().createCriteria(Technologie.class).add(Restrictions.ilike("nazev", "%" + nazev + "%")).list();
    }

    private BusinessCheckResult checkCanCreate(String name) {
        return new BusinessCheckSimpleResult(findByName(name) == null);
    }

    @Override
    public BusinessCheckResult checkCanDelete(Technologie t) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(Technologie t) {
        if (!currentSession().contains(t))
            currentSession().update(t);
    }
}

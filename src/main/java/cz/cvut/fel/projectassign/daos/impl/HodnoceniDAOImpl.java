/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.HodnoceniDAO;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databázi tykající se Hodnoceni
 * @author Tom
 */
@Repository
public class HodnoceniDAOImpl extends AbstractDAO<Hodnoceni, Integer> implements HodnoceniDAO, ICanDelete<Hodnoceni> {

    @Override
    public Hodnoceni create(String komentar, User user) {
        checkCanCreate(komentar, user.getId()).throwEx();
        
        Hodnoceni hodnoceni = new Hodnoceni(komentar, user);
        save(hodnoceni);
        return hodnoceni;
    }

    private BusinessCheckResult checkCanCreate(String komentar, int idUser) {
        return new BusinessCheckSimpleResult(true);
    }

    @Override
    public BusinessCheckResult checkCanDelete(Hodnoceni t) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(Hodnoceni h) {
        if (!currentSession().contains(h))
            currentSession().update(h);
    }
}

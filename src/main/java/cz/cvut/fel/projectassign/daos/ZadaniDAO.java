/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.Zadani;
import java.util.List;

/**
 * Rozhraní pro perzistetní vrstvu zabývající se Zadáním
 * @author Tom
 */
public interface ZadaniDAO extends IDAO<Zadani, Integer> {

    public Zadani create(String nazev, String popis, int stav);

    public List<Zadani> findByStatus(int status);

    public Zadani findByName(String name);

    public Zadani findIdenticalZadaniByParalelka(Zadani zadani, Paralelka par);

    public List<Zadani> findAllByNameStav(String nazev, String stav);
    
    public void attach(Zadani z);
}

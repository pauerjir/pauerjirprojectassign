/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import java.util.List;

/**
 * Rozhraní pro perzistetní vrstvu zabývající se Uživatelem
 * @author Tom
 */
public interface UserDAO  extends IDAO<User, Integer> {

    public User findByLogin(String loginName);

    public User findByName(String name);

    public User create(String userName, String heslo, String email, String jmeno, String prijmeni);
    
    public List<User> findAllByName(String search);
    
    public User findTutorByParalelka(Paralelka par);
    
    public void attach(User u);
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;

/**
 * Rozhraní pro perzistetní vrstvu zabývající se uživatelskými rolemi
 * @author Tom
 */
public interface UserRoleDAO  extends IDAO<UserRole, Integer> {

    public UserRole create(User user, int role);
    
    public void attach(UserRole ur);
}

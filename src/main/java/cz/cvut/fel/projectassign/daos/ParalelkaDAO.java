/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Paralelka;

/**
 * Rozhraní pro perzistetní vrstvu zabývající se Paralelkou
 * @author Tom
 */
public interface ParalelkaDAO extends IDAO<Paralelka, Integer> {

    public Paralelka create(int cislo);

    public Paralelka create(int cislo, int cas, int den);

    public Paralelka findByNumber(int cislo);
    
    public Paralelka findByRequestedZadaniId(int zadaniId);
    
    public void attach(Paralelka p);
}

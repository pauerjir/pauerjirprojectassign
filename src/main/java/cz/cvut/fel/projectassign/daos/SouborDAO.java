/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Soubor;

/**
 *  Rozhraní pro perzistetní vrstvu zabývající se Soubory
 * @author Tom
 */
public interface SouborDAO extends IDAO<Soubor, Integer> {

    public Soubor create(String cesta, String puvod);

    public Soubor findByCesta(String cesta);
    
    public void attach(Soubor s);
}

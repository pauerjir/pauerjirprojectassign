/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Technologie;
import java.util.List;

/**
 *  Rozhraní pro perzistetní vrstvu zabývající se Technologiemi
 * @author Tom
 */
public interface TechnologieDAO extends IDAO<Technologie, Integer> {

    public Technologie findByName(String name);

    public List<Technologie> findAllByName(String nazev);

    public Technologie create(String nazev);
    
    public void attach(Technologie h);
}

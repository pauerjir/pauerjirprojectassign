/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanComplexDelete;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.ZadaniDAO;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.Zadani;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databází tykající se zadání
 *
 * @author Tom
 */
@Repository
public class ZadaniDaoImpl extends AbstractDAO<Zadani, Integer> implements ZadaniDAO, ICanComplexDelete<Zadani> {

    @Override
    public Zadani create(String nazev, String popis, int stav) {
        checkCanCreate(nazev).throwEx();
        
        Zadani zadani = new Zadani(nazev, popis, stav);
        save(zadani);
        return zadani;
    }

    @Override
    public List<Zadani> findByStatus(int status) {
        List<Zadani> output = currentSession().createCriteria(Zadani.class).add(Restrictions.eq("stav", status)).list();
        return output;
    }

    @Override
    public Zadani findByName(String name) {
        Zadani output = (Zadani) currentSession().createCriteria(Zadani.class).add(Restrictions.eq("nazev", name)).uniqueResult();
        return output;
    }

    @Override
    public Zadani findIdenticalZadaniByParalelka(Zadani zadani, Paralelka par) {
        String sqlQuery = "SELECT id_Zadani FROM Zadani AS z NATURAL JOIN (SELECT * FROM Semestralka AS s NATURAL JOIN UserParalelka as p WHERE p.id_Paralelka=:paralelka) AS j  WHERE z.id_Zadani=:idzadani";
        Query query = currentSession().createSQLQuery(sqlQuery);
        query.setParameter("idzadani", zadani.getId());
        query.setParameter("paralelka", par.getId());
        if (query.uniqueResult() == null) {
            return null;
        }
        Integer i = Integer.valueOf(query.uniqueResult().toString());
        Zadani z = (Zadani) findById(i);
        return z;
    }

    @Override
    public List<Zadani> findAllByNameStav(String nazev, String stav) {
        return currentSession().createCriteria(Zadani.class).add(Restrictions.ilike("nazev", "%" + nazev + "%")).add(Restrictions.lt("stav", Integer.valueOf(stav))).list();
    }

    private BusinessCheckResult checkCanCreate(String nazev) {
        return new BusinessCheckSimpleResult(findByName(nazev) == null);
    }

    @Override
    public BusinessCheckResult checkCanDelete(Zadani z) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(Zadani z) {
        if (!currentSession().contains(z))
            currentSession().update(z);
    }

    @Override
    public void processPreDelete(Zadani objectToDelete) {
        for (User u : objectToDelete.getZadaniStudents()) {
            u.setZadani(null);
        }
    }

    @Override
    public void processPostDelete(Zadani deletedObject) {
    }
}

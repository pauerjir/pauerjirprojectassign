/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.ICanDelete;
import cz.cvut.fel.asf.persistence.IPersistable;
import cz.cvut.fel.asf.persistence.hibernate.AbstractDAO;
import cz.cvut.fel.projectassign.daos.SouborDAO;
import cz.cvut.fel.projectassign.model.Soubor;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

/**
 * Třída obstarávající perzistenční operace nad databází tykající se souborů
 * @author Tom
 */
@Repository
public class SouborDAOImpl extends AbstractDAO<Soubor, Integer> implements SouborDAO, ICanDelete<Soubor> {

    @Override
    public Soubor create(String cesta, String puvod) {
        checkCanCreate(cesta).throwEx();
        
        Soubor soubor = new Soubor(cesta, puvod);
        save(soubor);
        return soubor;
    }

    @Override
    public Soubor findByCesta(String cesta) {
        Soubor output = (Soubor) currentSession().createCriteria(Soubor.class).add(Restrictions.eq("cesta", cesta)).uniqueResult();
        return output;
    }

    private BusinessCheckResult checkCanCreate(String cesta) {
        return new BusinessCheckSimpleResult(findByCesta(cesta) == null);
    }

    @Override
    public BusinessCheckResult checkCanDelete(Soubor soub) {
        return new BusinessCheckSimpleResult(true);
    }
    
    @Override
    public void attach(Soubor s) {
        if (!currentSession().contains(s))
            currentSession().update(s);
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.daos;

import cz.cvut.fel.asf.persistence.IDAO;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;

/**
 * Rozhraní pro perzistetní vrstvu zabývající se Hodnocením
 * @author Tom
 */
public interface HodnoceniDAO extends IDAO<Hodnoceni, Integer> {

    public Hodnoceni create(String komentar, User user);
    
    public void attach(Hodnoceni h);
}

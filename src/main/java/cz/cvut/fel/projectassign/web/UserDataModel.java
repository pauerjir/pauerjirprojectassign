/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.services.UserService;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 * umožnuje z tabulky ziskat data o záznamu
 * @author Tom
 */
public class UserDataModel extends ListDataModel<User> implements SelectableDataModel<User> {

    private UserService userService;

    public UserDataModel() {
    }

    public UserDataModel(List<User> data, UserService service) {
        super(data);
        userService = service;
    }

    @Override
    public User getRowData(String rowKey) {
        return userService.findById(Integer.parseInt(rowKey));
    }

    @Override
    public Object getRowKey(User user) {
        return user.getId();
    }
}

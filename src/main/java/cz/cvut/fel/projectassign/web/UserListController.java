/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.services.HodnoceniService;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import cz.cvut.fel.projectassign.services.UserService;
import java.util.SortedMap;
import java.util.TreeMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od uživatelů
 * a volá metody servisní vrstvy. Zabývá se prevážně seznamem se uživateli. 
 * @author Tom
 */
@Component("userListController")
@Scope("session")
public class UserListController extends AbstractController {

    @Autowired
    private UserService userService;
    @Autowired
    private ParalelkaService parService;
    @Autowired
    private HodnoceniService hodService;
    
    private User selectedUser;
    private UserDataModel userDataModel;
    private String filterName="";
    private String filterStatus="";
    private Hodnoceni hodnoceni;
    private User user;
     private int rows=10;
    private SortedMap<String,String> paralelky = new TreeMap<String, String>();
    private boolean newSearch=true;


    
    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public UserDataModel getUserDataModel() {
        if (userDataModel == null) {
            rebindData();
        }
        return userDataModel;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String name) {
        this.filterName = name;
    }

    public User getSelectedUser() {
        return selectedUser;
    }

    public void setSelectedUser(User selectedUser) {
        this.selectedUser = selectedUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Hodnoceni getHodnoceni() {
        return hodnoceni;
    }

    public void setHodnoceni(Hodnoceni hodnoceni) {
        this.hodnoceni = hodnoceni;
    }

    
    public SortedMap<String,String> getParalelky() {
        paralelky=parService.setMapParalelky();
        return paralelky;
       
    }

    public void setParalelky(SortedMap<String,String> paralelky) {
        this.paralelky = paralelky;
    }

    public String getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(String filterStatus) {
            this.filterStatus = filterStatus;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="views">
    public String hodnoceni() {
        return "hodnoceni.xhtml?id=" + selectedUser.getId() + "&faces-redirect=true";
    }
    
     public String detailZadani() {
        return "detailZadani.xhtml?id=" + selectedUser.getZadani().getId() + "&faces-redirect=true";
    }

    public String search() {
        String popis = (filterName == null) ? "" : filterName.trim();
        String status = (filterStatus == null) ? "" : filterStatus.trim();
        newSearch=true;
        
        return "userList.xhtml?name=" + popis +"&paralelka=" +status +"&faces-redirect=true";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="inicializacni methody">
    /**
     * Provede změnu obsahu, pokud tak ma učinit
     * @param filterName
     * @param filterStatus 
     */
    public void getFilterParameters(String filterName, String filterStatus) {
      if(newSearch){
        if(filterStatus!=null&& !filterStatus.equals("")){
         this.filterStatus=filterStatus;
        }else{
        this.filterStatus="";
        }
        this.filterName = filterName;
        newSearch=false;
        rebindData();

      }
        
    }
    
        public void findUserById(String id) {
        if (!id.equals("")) {
            user = userService.findById(Integer.parseInt(id));
            hodnoceni=hodService.findById(user.getHodnoceni().getId());
        }
    }
    
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="helper method">


    private void rebindData() {
        
        userDataModel = new UserDataModel(userService.getAllStudentsByFilter(filterName,filterStatus), userService);
    }

    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="visibility">
        public boolean hodnoceniDisable(){
            return selectedUser==null;
        }
        
        public boolean detailDisable(){
            return (selectedUser==null|| selectedUser.getZadani()==null);
        }
        //</editor-fold>
    
    public void init() {
        if (isRebindFormDataEnabled())
            rebindData();
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 * umožnuje z tabulky ziskat data o záznamu
 * @author Tom
 */
public class ParalelkaDataModel extends ListDataModel<Paralelka> implements SelectableDataModel<Paralelka> {

    private ParalelkaService paralelkaService;

    public ParalelkaDataModel() {
    }

    public ParalelkaDataModel(List<Paralelka> data, ParalelkaService service) {
        super(data);
        paralelkaService = service;
    }

    @Override
    public Paralelka getRowData(String rowKey) {
        return paralelkaService.findById(Integer.parseInt(rowKey));
    }

    @Override
    public Object getRowKey(Paralelka paralelka) {
        return paralelka.getId();
    }
}

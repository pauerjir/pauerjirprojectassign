/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web.notifications;

import cz.cvut.fel.asf.notifications.template.NotificationBase;
import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author balikm1
 */
public class TutorCreatedNotification extends NotificationBase {
    
    private final String NIS_PASSWD_MSG = "Heslo použij stejné jako v učebně na terminálech SUN";
    
    private User mUser;
    private String mPassword;
    private boolean mUseDbPasswd;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    public TutorCreatedNotification(User user, String password, boolean useDbPasswd) {
        mUser = user;
        mPassword = password;
        mUseDbPasswd = useDbPasswd;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Interface implementation">
    @Override
    protected String getCode() {
        return "TutorCreated";
    }

    @Override
    protected List<InternetAddress> getRecipients() throws AddressException {
        List<InternetAddress> l = new ArrayList<InternetAddress>();
        l.add(new InternetAddress(mUser.getEmail()));
        return l;
    }

    @Override
    protected Map<String, String> getReplacements() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("%WEBSITE_ROOTURL%", "https://edux.feld.cvut.cz/ProjectAssignPR1/" /*AbstractController.getRootUrl()*/);
        map.put("%USER_NAME%", String.format("%s %s", mUser.getJmeno(), mUser.getPrijmeni()));
        map.put("%LOGIN_NAME%", mUser.getUserName());
        map.put("%PASSWORD%", mUseDbPasswd ? mPassword : NIS_PASSWD_MSG);
        return map;
    }
    //</editor-fold>
}

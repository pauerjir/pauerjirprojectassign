/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import cz.cvut.fel.projectassign.services.UserService;
import java.util.Date;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author balikm1
 */
@Component("paralelkaController")
@Scope("request")
public class ParalelkaController extends AbstractController {
    
    @Autowired
    ParalelkaService paralelkaService;
    @Autowired
    UserService userService;
    
    private Paralelka paralelka;
    private Date deadlineVyber;
    
    public Paralelka getParalelka() {
        if (paralelka == null) {
            Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String id = parameterMap.get("id");

            if (id != null) {
                try {
                    paralelka = paralelkaService.findById(Integer.valueOf(id));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        
        return paralelka;
    }

    public void setParalelka(Paralelka paralelka) {
        this.paralelka = paralelka;
    }
    
    public String getParalelkaTime() {
        return paralelkaService.getCasString(getParalelka());
    }
    
    public String getParalelkaDay() {
        return paralelkaService.getDayString(getParalelka());
    }
    
    public Date getDeadlineVyber() {
        return deadlineVyber;
    }

    public void setDeadlineVyber(Date deadlineVyber) {
        this.deadlineVyber = deadlineVyber;
    }
    
    /**
     * upraví zvolenou paralelku
     *
     * @return
     */
    public String update() {
        paralelkaService.update(getParalelka());
        
        FacesMessage msg = new FacesMessage("Paralelka číslo: " + getParalelka().getCisloParalelky() + " byla upravena.");
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return String.format("paralelka.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    
    public String back() {
        return "paralelka.xhtml?faces-redirect=true";
    }
    
    public void init() {
        if (getParalelka() == null || (!userService.getCurrentUser().hasRole(UserRole.TUTOR) && !userService.getCurrentUser().hasRole(UserRole.ADMIN))) {
            pageRedirect("/index.xhtml");
        }
    }
}

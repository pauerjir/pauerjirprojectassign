/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

/**
 *
 * @author balikm1
 */
public class ZadaniDataModelRecord {
    private Integer id;
    private String nazev;
    private String statusText;
    private int stav;
    private boolean hasStudents;

    public ZadaniDataModelRecord(Integer id, String nazev, String statusText, int stav, boolean hasStudents) {
        this.id = id;
        this.nazev = nazev;
        this.statusText = statusText;
        this.stav = stav;
        this.hasStudents = hasStudents;
    }
    
    public Integer getId() {
        return id;
    }

    public String getNazev() {
        return nazev;
    }

    public String getStatusText() {
        return statusText;
    }
    
    public int getStav() {
        return stav;
    }
    
    public boolean hasStudents() {
        return hasStudents;
    }
}

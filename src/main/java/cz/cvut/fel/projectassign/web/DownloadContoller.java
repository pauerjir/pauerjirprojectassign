/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.model.Soubor;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.services.SouborService;
import cz.cvut.fel.projectassign.services.UserService;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od uživatelů
 * a volá metody servisní vrstvy. Zabývá se downloadem. 
 * @author Tom
 */
@ApplicationScoped
@Component("downloadController")
public class DownloadContoller {

    @Autowired
    private UserService userService;
    @Autowired
    private SouborService souboryService;

    //<editor-fold defaultstate="collapsed" desc="download">
   /**
    * Vrací ke stažení zvolený soubor
    * @param file
    * @return StreamedContent
    */
    public StreamedContent download(File file) {
        try {
            return souboryService.downloadFile(file);
        } catch (FileNotFoundException ex) {

            Logger.getLogger(DownloadContoller.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
/**
 * Vrací zazipované soubory zvoleného uživatele
 * @param user
 * @return StreamedContent
 */
    public StreamedContent zipAllNewestFilesByType(User user) {
        return souboryService.zipAllNewestFilesByType(user);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="pomocne metody">
      /**
     * Zjistí původní jméno souboru
     * @param file
     * @return String
     */
    public String getPuvJmeno(File file) {
        Soubor soubor = souboryService.findByCesta("/semestralky/" + userService.getCurrentUser().getUserName() + "/" + file.getName());
        if (soubor == null) {
            return "Soubor existuje ale nebyl nalezen v databázi";
        }
        return soubor.getPuvodniJmeno();
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="visibility">
    public boolean downloadDisable(User user) {
        return souboryService.downloadDisable(user);

    }
    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.services.TechnologieService;
import java.util.List;
import javax.faces.model.ListDataModel;
import org.primefaces.model.SelectableDataModel;

/**
 * umožnuje z tabulky ziskat data o záznamu
 * @author Tom
 */
public class TechnologieDataModel extends ListDataModel<Technologie> implements SelectableDataModel<Technologie> {

    private TechnologieService technologieService;

    public TechnologieDataModel() {
    }

    public TechnologieDataModel(List<Technologie> data, TechnologieService service) {
        super(data);
        technologieService = service;
    }

    @Override
    public Technologie getRowData(String rowKey) {
        return technologieService.findById(Integer.parseInt(rowKey));
    }

    @Override
    public Object getRowKey(Technologie technologie) {
        return technologie.getId();
    }
}

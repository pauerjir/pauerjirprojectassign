/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web.notifications;

import cz.cvut.fel.asf.notifications.template.NotificationBase;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.Zadani;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author balikm1
 */
public class ProjectCanceledNotification extends NotificationBase {
    private User user;
    private Zadani zadani;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    public ProjectCanceledNotification(User user, Zadani zadani) {
        this.user = user;
        this.zadani = zadani;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Interface implementation">
    @Override
    protected String getCode() {
        return "ProjectCanceled";
    }

    @Override
    protected List<InternetAddress> getRecipients() throws AddressException {
        List<InternetAddress> l = new ArrayList<InternetAddress>();
        l.add(new InternetAddress(user.getEmail()));
        return l;
    }

    @Override
    protected Map<String, String> getReplacements() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("%WEBSITE_ROOTURL%", "https://edux.feld.cvut.cz/ProjectAssignPR1/" /*AbstractController.getRootUrl()*/);
        map.put("%USER_NAME%", String.format("%s %s", user.getJmeno(), user.getPrijmeni()));
        map.put("%ZADANI_NAME%", (zadani != null) ? zadani.getNazev() : "");
        return map;
    }
    //</editor-fold>
}

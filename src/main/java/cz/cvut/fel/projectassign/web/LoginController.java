/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od uživatelů
 * a volá metody servisní vrstvy. Zabývá se přihlášením do aplikace. 
 * @author Tom
 */
@Component("loginController")
public class LoginController implements Serializable {

    private String password = "";
    private String userName = "";
    
    public String getSubjectName() {
        return FacesContext.getCurrentInstance().getExternalContext().getInitParameter("projectassign.subjectname");
    }
    
    public String getSubjectCode() {
        return FacesContext.getCurrentInstance().getExternalContext().getInitParameter("projectassign.subjectcode");
    }

    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="login">
    /**
     * provede login
     * @throws IOException
     * @throws ServletException
     */
    public void login() throws IOException, ServletException {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest()).getRequestDispatcher("/j_spring_security_check?j_username=" + getUserName() + "&j_password=" + getPassword());
        dispatcher.forward((ServletRequest) context.getRequest(), (ServletResponse) context.getResponse());
        FacesContext.getCurrentInstance().responseComplete();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="pomocné metody">
    /**
     * zajistuje nepredvyplneni u logování
     */
    public void reset() {
        password = "";
        userName = "";
       
    }
    //</editor-fold>
}
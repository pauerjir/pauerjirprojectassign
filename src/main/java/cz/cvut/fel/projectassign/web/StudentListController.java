package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.tools.StringHelper;
import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.asf.web.primefaces.QueryLazyDataModel;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.queries.StudentListQuery;
import cz.cvut.fel.projectassign.queries.StudentListQueryRecord;
import cz.cvut.fel.projectassign.services.HodnoceniService;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import cz.cvut.fel.projectassign.services.SouborService;
import cz.cvut.fel.projectassign.services.UserService;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import org.primefaces.model.StreamedContent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni seznamu studentů. 
 * @author balikm1
 */
@Component("studentListController")
@Scope("session")
public class StudentListController extends AbstractController {
    
    private static final int CIRCLES_ALL = -1;
    private static final int CIRCLES_BY_USER = -2;
    
    private static final int DEFAULT_ROWS_COUNT = 10;

    @Autowired
    private UserService userService;
    @Autowired
    private ParalelkaService parService;
    @Autowired
    private SouborService souboryService;
    
    private StudentListQueryRecord selectedRecord;
    private QueryLazyDataModel<StudentListQueryRecord> studentListDataModel;
    private String filterName = null;
    private Integer filterCircles = null;
    private SortedMap<String, Integer> circlesDataSource;
    private Integer rows = null;
    private boolean newSearch=true;


    
    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public QueryLazyDataModel<StudentListQueryRecord> getStudentListDataModel() {
        if (studentListDataModel == null) {
            rebindData();
        }
        return studentListDataModel;
    }

    public String getFilterName() {
        return filterName != null ? filterName.trim() : "";
    }

    public void setFilterName(String name) {
        this.filterName = name;
    }
    
    public Integer getFilterCircles() {
        return filterCircles != null ? filterCircles : CIRCLES_BY_USER;
    }

    public void setFilterCircles(Integer filterCircles) {
            this.filterCircles = filterCircles;
    }

    public StudentListQueryRecord getSelectedRecord() {
        return selectedRecord;
    }

    public void setSelectedRecord(StudentListQueryRecord selectedRecord) {
        this.selectedRecord = selectedRecord;
    }

    public SortedMap<String, Integer> getCirclesDataSource() {
        if (circlesDataSource == null) {
            circlesDataSource = new TreeMap<String, Integer>();
            for (Paralelka p : parService.getAllParalelka()) {
                circlesDataSource.put(Integer.toString(p.getCisloParalelky()), p.getCisloParalelky());
            }
        }
        circlesDataSource.put("> Všechny paralelky <", CIRCLES_ALL);
        circlesDataSource.put("> Moje paralelky <", CIRCLES_BY_USER);
        return circlesDataSource;
    }
    
    public int getRows() {
        return rows != null ? rows : DEFAULT_ROWS_COUNT;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="views">
    public String hodnoceni() {
        return "hodnoceni.xhtml?id=" + selectedRecord.getId() + "&faces-redirect=true";
    }
    
     public String detailZadani() {
        return "detailZadani.xhtml?id=" + selectedRecord.getProjectTopicId() + "&faces-redirect=true";
    }

    public String search() {
        newSearch=true;
        
        return String.format("userList.xhtml?name=%s&paralelka=%d&faces-redirect=true", getFilterName(), getFilterCircles());
    }
    
    /**
    * Vrací zazipované soubory zvoleného uživatele
    * @param userName
    * @return StreamedContent
    */
    public StreamedContent zipAllNewestFilesByType(String userName) {
        return souboryService.zipAllNewestFilesByType(userService.getUserByLogin(userName));
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="inicializacni methody">
    /**
     * Provede změnu obsahu, pokud tak ma učinit
     * @param filterName
     * @param filterCircles 
     */
    public void getFilterParameters(String filterName, String filterCircles) {
      if(newSearch){
        if(!StringHelper.isNullOrEmpty(filterCircles)){
         this.filterCircles=Integer.parseInt(filterCircles);
        }
        this.filterName = filterName;
        newSearch=false;
        rebindData();

      }
        
    }
    
        
    
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="helper methods">


    private void rebindData() {
        studentListDataModel = new QueryLazyDataModel<StudentListQueryRecord>(new StudentListQuery(filterName, getSelectedCirclesList()));
    }
    
    private List<Paralelka> getSelectedCirclesList() {
        switch (getFilterCircles()) {
            case CIRCLES_ALL:
                return null;
            case CIRCLES_BY_USER:
                System.out.println("Paralelky: " + userService.getCurrentUser().getParalelka());
                return new ArrayList(userService.getCurrentUser().getParalelka());
            default:
                List<Paralelka> circles = new ArrayList<Paralelka>();
                circles.add(parService.getByClassNumber(getFilterCircles()));
                return circles;
        }
    }

    //</editor-fold>
        
    //<editor-fold defaultstate="collapsed" desc="visibility">
        public boolean hodnoceniDisable(){
            return selectedRecord == null;
        }
        
        public boolean detailDisable(){
            return (selectedRecord == null || selectedRecord.getProjectTopicId() == null);
        }
        
        public boolean downloadDisable(String userName) {
            return souboryService.downloadDisable(userService.getUserByLogin(userName));
        }
        //</editor-fold>
    
    public void init() {
        if (isRebindFormDataEnabled())
            rebindData();
    }
}

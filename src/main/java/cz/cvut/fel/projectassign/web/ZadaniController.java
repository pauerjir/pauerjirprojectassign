/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.*;
import cz.cvut.fel.projectassign.services.TechnologieService;
import cz.cvut.fel.projectassign.services.UserService;
import cz.cvut.fel.projectassign.services.ZadaniService;
import java.io.IOException;
import java.util.*;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně zadáními.
 *
 * @author Tom
 */
@Component("zadaniController")
@Scope("request")
public class ZadaniController extends AbstractController {

    @Autowired
    private ZadaniService zadaniService;
    @Autowired
    private UserService userService;
    @Autowired
    private TechnologieService techService;
    
    private Zadani zadani;
    private List<Technologie> selectedTech;
    private String nazev;
    private String popis;

    //<editor-fold defaultstate="collapsed" desc="views">
    /**
     * zarezervuje zvolené zadáni
     *
     * @return
     */
    public String rezervovat() {
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                zadaniService.rezervovatZadani(getZadani(), userService.getCurrentUser());
            }
        })) {
            FacesMessage msg = new FacesMessage("Zadání \"" + getZadani().getNazev() + "\" bylo rezervováno!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }

    public String schvalit() {
        return "schvalitZadani.xhtml?faces-redirect=true";
    }

    public String detail() {
        return "detailZadani.xhtml?id=" + getZadani().getId() + "&faces-redirect=true";
    }

    public String semestralka() {
        return "semestralka.xhtml?faces-redirect=true";
    }

    public String create() {
        return "createZadani.xhtml?faces-redirect=true";
    }

    public String edit() throws IOException {
        return "editZadani.xhtml?id=" + getZadani().getId() + "&faces-redirect=true";
    }

    /**
     * smaze zvolené zadani
     */
    public String delete() {
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                zadaniService.deleteZadani(getZadani(), userService.getCurrentUser());
            }
        })) {
            FacesMessage msg = new FacesMessage("Zadání bylo smazáno!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }
        
        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }

    /**
     * Smaze vyber semestrální práce aktualniho uzivatele
     */
    public String deleteSemestralka() {
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                if (getZadani().getStav() == 1) {
                    zadaniService.deleteSemestralka(userService.getCurrentUser());
                } else {
                    zadaniService.deleteZadani(getZadani(), userService.getCurrentUser());
                }
            }
        })) {
            FacesMessage msg = new FacesMessage("Váš výběr semestrální práce byl úspěšně zrušen!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    
    public String addAsRequested() {
        return add(ZadaniStatus.REQUESTED);
    }

    /**
     * vytvoři nové zadání
     * @param stav
     */
    public String add(final int stav) {
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                zadani = zadaniService.createByUser(getNazev(), getPopis(), stav, userService.getCurrentUser(), getSelectedTech());
            }
        })) {
            FacesMessage msg = new FacesMessage("Zadání " + getZadani().getNazev() + " bylo vytvořeno.");
            FacesContext.getCurrentInstance().addMessage(null, msg);
        }

        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    
    public String updateAsRequested() {
        return update(ZadaniStatus.REQUESTED);
    }

    /**
     * Upraví zvolené zadání
     * @param stav
     */
    public String update(int stav) {
        zadaniService.updateStatus(getZadani(), stav);
        
        FacesMessage msg = new FacesMessage("Zadani " + getZadani().getNazev() + " bylo upraveno.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        
        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="features visibility">
    public boolean acceptDisabled() {
        return (getZadani() == null || getZadani().getStav() != ZadaniStatus.REQUESTED);
    }

    public boolean createDisabled() {
        return zadaniService.createDisabled(userService.getCurrentUser());
    }

    public boolean editDisabled() {
        return zadaniService.editDisabled(getZadani(), userService.getCurrentUser());
    }

    public boolean semestralkaDisable() {
        return (userService.getCurrentUser().getZadani() == null);
    }

    public boolean detailDisabled() {
        return getZadani() == null;
    }

    public boolean deleteDisabled() {
        return !getZadani().getZadaniStudents().isEmpty();
    }

    public boolean reservationDisabled() {
        return zadaniService.reservationDisabled(getZadani(), userService.getCurrentUser());
    }

    public boolean deleteSemestralkaDisabled() {
        return zadaniService.deleteSemestralkaDisabled(getZadani(), userService.getCurrentUser());
    }

    public boolean potvrditDisable() {

        return zadaniService.potvrditDisable(getZadani(), userService.getCurrentUser());
    }

    public boolean odevzdatDisable() {
        User user = userService.getCurrentUser();
        return (!user.hasRole(UserRole.USER)
             || user.getZadani() == null
             || user.getStudentsParalelka().getDeadlineVyber().after(new Date()));
    }

    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public Zadani getZadani() {
        if (zadani == null) {
            String id = getRequestParameter("id");

            if (id != null) {
                try {
                    zadani = zadaniService.findById(Integer.valueOf(id));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        return zadani;
    }

    public void setZadani(Zadani zadani) {
        this.zadani = zadani;
    }
    
    public List<Technologie> getSelectedTech() {
        return getZadani() != null ? new ArrayList<Technologie>(getZadani().getTechnologies()) : selectedTech/*new ArrayList<Technologie>()*/;
    }
    
    public void setSelectedTech(List<Technologie> tech) {
        if (getZadani() != null) {
            getZadani().setTechnologies(new HashSet<Technologie>(tech));
        } else {
            selectedTech = tech;
        }
    }
    
    public List<Technologie> getAllTechnologies() {
        return techService.getAllTechnologie();
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="helper methods">
    
    /**
     * zobrazí uvodni info
     */
    public String infoIntro() {
        if (!userService.getCurrentUser().hasRole(UserRole.USER)) {
            return "";
        }
        long vybrat = userService.getCurrentUser().getStudentsParalelka().getDeadlineVyber().getTime();
        long aktTime = Calendar.getInstance().getTime().getTime();


        if (vybrat > aktTime) {
            return "Na výběr zadaní máš čas do: " + userService.getCurrentUser().getStudentsParalelka().getDeadlineVyber();
        } else {
            return "Již nelze měnit vybrané zadání";
        }
    }
    
    public void validateRights() {
        if (getZadani() == null) {
            pageRedirect("/createZadani.xhtml");
        }
        User user = userService.getCurrentUser();
        if (!user.hasRole(UserRole.TUTOR) && !user.hasRole(UserRole.ADMIN)) {
            if (!getZadani().equals(user.getZadani())) {
                pageRedirect("/index.xhtml");
            }
        }
    }
    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import java.util.Date;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně seznamem s
 * paralelkami.
 *
 * @author Tom
 */
@Component("paralelkaListController")
@Scope("session")
public class ParalelkaListController extends AbstractController {

    @Autowired
    private ParalelkaService paralelkaService;
    private Paralelka selectedParalelka;
    private ParalelkaDataModel paralelkaDataModel;
    private String filterName = "";
    private int rows = 10;

    //<editor-fold defaultstate="collapsed" desc="views">
    /**
     * Přesměruje na stánku s editací podle zvolené paralelky
     *
     * @return
     */
    public String edit() {
        return "editParalelka.xhtml?id=" + selectedParalelka.getId() + "&faces-redirect=true";
    }

    /**
     * nastaví vyhledávání
     *
     * @return
     */
    public String search() {
        String popis = (filterName == null) ? "" : filterName.trim();
        return "paralelka.xhtml?name=" + popis + "&faces-redirect=true";
    }

    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="pomocne metody">
    private void rebindData() {
        paralelkaDataModel = new ParalelkaDataModel(paralelkaService.getAllParalelka(), paralelkaService);
    }

    public ParalelkaDataModel getParalelkaDataModel() {
        if (paralelkaDataModel == null) {
            rebindData();
        }
        return paralelkaDataModel;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="features visibility">
    public boolean edidDisabled() {
        return (selectedParalelka == null);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String filterName) {
        this.filterName = filterName;
    }

    

    public Paralelka getSelectedParalelka() {
        return selectedParalelka;
    }

    public void setSelectedParalelka(Paralelka selectedParalelka) {
        this.selectedParalelka = selectedParalelka;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    
    
    

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="inicializacni methody">
    /**
     * Uloží si data z filtru a vyhledá nové udaje
     *
     * @param filterName
     */
    public void getFilterParameters(String filterName) {
        this.filterName = filterName;
        rebindData();
    }

    /**
     * Vrátí slobně den paralelky
     *
     * @param paralelka
     * @return String
     */
    public String getDayString(Paralelka paralelka) {
        return paralelkaService.getDayString(paralelka);
    }

    /**
     * Vrátí slobně čas paralelky
     *
     * @param paralelka
     * @return String
     */
    public String getTimeString(Paralelka paralelka) {
        return paralelkaService.getCasString(paralelka);
    }
    //</editor-fold>
    
    public void init() {
        if (isRebindFormDataEnabled())
            rebindData();
    }
}

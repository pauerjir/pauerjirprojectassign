/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.businesschecks.BusinessCheckException;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.tools.StringHelper;
import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Soubor;
import cz.cvut.fel.projectassign.model.Zadani;
import cz.cvut.fel.projectassign.services.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně uploadem.
 *
 * @author Tom
 */
@Component("uploadController")
@Scope("singleton")
public class UploadController extends AbstractController {

    @Autowired
    private UserService serviceUser;
    @Autowired
    private ZadaniService serviceZadani;
    @Autowired
    private TechnologieService serviceTech;
    @Autowired
    private SouborService serviceSoubor;
    @Autowired
    private KosHelperService serviceKosHelper;
    public UploadedFile fileToUpload;
    private List<File> fileListPdf;
    private List<File> fileListSql;
    private List<File> fileListSemestralka;
    private List<File> fileListEr;
    private List<File> fileListJavadoc;
    private Soubor.typSoubor typ;
    private String vyberSoubor = "1";
    private String hideSelect = "";
    private String hideUpload = "hidden";
    private String hideGit = "hidden";

    //<editor-fold defaultstate="collapsed" desc="getters file lists">
    public List<File> getFileListPdf() {
        fileListPdf = new ArrayList<File>();
        File f;
        if ((f = serviceSoubor.getFileNew(serviceUser.getCurrentUser(), ".pdf")) != null) {
            fileListPdf.add(f);
        }

        return fileListPdf;
    }

    public List<File> getFileListJavadoc() {
        fileListJavadoc = new ArrayList<File>();
        File f;
        if ((f = serviceSoubor.getFileNew(serviceUser.getCurrentUser(), "jd.zip")) != null) {
            fileListJavadoc.add(f);
        }
        return fileListJavadoc;
    }

    public List<File> getFileListZip() {
        fileListSemestralka = new ArrayList<File>();
        File f;
        if ((f = serviceSoubor.getFileNew(serviceUser.getCurrentUser(), "sp.zip")) != null) {
            fileListSemestralka.add(f);
        }
        return fileListSemestralka;
    }

    public List<File> getFileListSql() {
        fileListSql = new ArrayList<File>();
        File f;
        if ((f = serviceSoubor.getFileNew(serviceUser.getCurrentUser(), ".sql")) != null) {
            fileListSql.add(f);
        }
        return fileListSql;
    }

    public List<File> getFileListEr() {
        fileListEr = new ArrayList<File>();
        File f;
        if ((f = serviceSoubor.getFileNew(serviceUser.getCurrentUser(), ".jpg")) != null) {
            fileListEr.add(f);
        }
        return fileListEr;
    }

    public Soubor.typSoubor getTyp() {
        switch (Integer.valueOf(getVyberSoubor())) {
            case 1:
                typ = Soubor.typSoubor.SEMESTRALKA;
                break;
            case 2:
                typ = Soubor.typSoubor.DOKUMENTACE;
                break;
            case 3:
                typ = Soubor.typSoubor.JAVADOC;
                break;
            case 4:
                typ = Soubor.typSoubor.SQL;
                break;
            case 5:
                typ = Soubor.typSoubor.ER_DIAGRAM;
                break;
            case 6:
                typ = Soubor.typSoubor.GIT;
                break;
        }

        return typ;
    }

    public void setTyp(Soubor.typSoubor typ) {
        this.typ = typ;
    }

    public String getVyberSoubor() {
        return vyberSoubor;
    }

    public void setVyberSoubor(String vyberSoubor) {
        this.vyberSoubor = vyberSoubor;
    }

    public String getHideSelect() {
        return hideSelect;
    }

    public void setHideSelect(String hideSelect) {
        this.hideSelect = hideSelect;
    }

    public String getHideUpload() {
        return hideUpload;
    }

    public void setHideUpload(String hideUpload) {
        this.hideUpload = hideUpload;
    }

    public String getHideGit() {
        return hideGit;
    }

    public void setHideGit(String hideGit) {
        this.hideGit = hideGit;
    }
    
    public String getHideFile() {
        if(hideUpload.equals("")&&hideGit.equals("hidden")){
            return "hidden";
        }
        return "";
    }

    //</editor-fold>
    public UploadedFile getFileToUpload() {
        return this.fileToUpload;
    }

    public void setFileToUpload(UploadedFile file) {

        this.fileToUpload = file;
    }

    //<editor-fold defaultstate="collapsed" desc="upload kos user + paralelka">
    /**
     * vytvoří záznamy ziskané z kosu
     */
    public void uploadKosFile() {
        FacesMessage msg = new FacesMessage("Vytvářím databázovou strukturu.");
        FacesContext.getCurrentInstance().addMessage("neco", msg);
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                try {
                    serviceKosHelper.parseKosFile(fileToUpload.getInputstream());
                } catch (IOException ex) {
                    Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
                    new BusinessCheckSimpleResult(false, "Chyba čtení souboru: %s", ex.getMessage()).throwEx();
                } finally {
                    FacesMessage msg = new FacesMessage(String.format("Bylo vytvořeno %d uživatelů.", serviceKosHelper.getUsersCreated()));
                    FacesContext.getCurrentInstance().addMessage("neco", msg);
                }
            }
        })) {
            msg = new FacesMessage("Povedlo se: Uživatelé byli vytvoření.");
            FacesContext.getCurrentInstance().addMessage("neco", msg);
        }
    }

    @Override
    protected void processExecuteErrorMessages(String[] messages) {
        for (String s : messages) {
            FacesMessage msg = new FacesMessage("Chyba: " + s);
            FacesContext.getCurrentInstance().addMessage("neco", msg);
        }
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="upload zadani">
    public void uploadZadani() throws IOException {

        try {
            parseZadani(fileToUpload);
        } catch (Exception ex) {
            Logger.getLogger(UploadController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void parseZadani(UploadedFile fileUp) throws Exception {

        InputStream in = fileUp.getInputstream();
        StringBuilder sb = new StringBuilder();

        String s = null;
        int c = -1;

        String nazev = null;
        String popis = null;
        boolean isNazev = true;
        boolean isStart = true;

        do {
            c = in.read();
            sb.append((char) c);

            if (isStart && (s = sb.toString()).contains("\r\n")) {
                s = s.substring(0, s.indexOf("\r\n"));
                sb = sb.delete(0, s.length() + 2);
                isStart = false;
            }

            if (isNazev && (s = sb.toString()).contains("\r\n")) {
                s = s.substring(0, s.indexOf("\r\n"));
                sb = sb.delete(0, s.length() + 2);
                nazev = s;
                isNazev = false;

                cistOdradkování(in);
            }
            if ((s = sb.toString()).contains("=================================")) {
                s = s.substring(0, s.indexOf("================================="));
                sb = sb.delete(0, s.length() + 33);
                popis = s;

                cistOdradkování(in);
            }
            if (nazev != null & popis != null) {

                serviceZadani.create(nazev, popis, 1);
                popis = null;
                nazev = null;
                isNazev = true;
                isStart = true;
            }

        } while (c != -1);

    }

    private void cistOdradkování(InputStream in) throws IOException {
        for (int i = 0; i < 2; i++) {
            in.read();
        }

    }

    //</editor-fold>
    // <editor-fold defaultstate="collapsed" desc="upload semestralky">
    /**
     * nahraje semestrální práci
     *
     * @param event
     * @return
     */
    public String uploadSemestralka(FileUploadEvent event) {

        serviceSoubor.uploadSemestralky(event, serviceUser.getCurrentUser(), getTyp());
        hideSelect = "";
        hideUpload = "hidden";
        return "uploadSemestralka.xhtml?faces-redirect=true";
    }

    public String checkTypes() {
        String omezeni = "";
        switch (Integer.valueOf(getVyberSoubor())) {
            case 1:
                omezeni = "(.zip)";
                break;
            case 2:
                omezeni = "(.pdf)";
                break;
            case 3:
                omezeni = "(.zip)";
                break;
            case 4:
                omezeni = "(.sql)";
                break;
            case 5:
                omezeni = "(.jpg)";
                break;
        }

        return "/" + omezeni + "$/";
    }

    public void refresh() {
        vyberSoubor = getVyberSoubor();
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="visibility">
    public String haveSql() {
        if (serviceUser.getCurrentUser().getZadani() != null && (serviceUser.getCurrentUser().getZadani().getTechnologies().contains(serviceTech.findByName("SQL")) || serviceUser.getCurrentUser().getZadani().getPopis().contains("sql"))) {
            return "";
        } else {
            return "hidden";
        }
    }

    public String showUpload() {
        hideSelect = "hidden";
        hideUpload = "";
        if (typ == Soubor.typSoubor.GIT) {
            hideGit = "";
        }
        return "uploadSemestralka.xhtml?faces-redirect=true";
    }

    public String showSelectTypes() {
        hideSelect = "";
        hideGit = "hidden";
        hideUpload = "hidden";
        return "uploadSemestralka.xhtml?faces-redirect=true";
    }

    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.TechnologieService;
import cz.cvut.fel.projectassign.services.UserService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 *
 * @author balikm1
 */
@Component("technologieController")
@Scope("request")
public class TechnologieController extends AbstractController {
    
    @Autowired
    TechnologieService technologieService;
    @Autowired
    UserService userService;
    
    private Technologie technologie;
    private String nazev;
    
    public Technologie getTechnologie() {
        if (technologie == null) {
            String id = getRequestParameter("id");

            if (id != null) {
                try {
                    technologie = technologieService.findById(Integer.valueOf(id));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        
        return technologie;
    }

    public void setTechnologie(Technologie technologie) {
        this.technologie = technologie;
    }
    
    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
    
    /**
     * přidá zvolenou technologii
     *
     * @return
     */
    public String add() {
        technologie = technologieService.createTechnologie(getNazev());
        
        FacesMessage msg = new FacesMessage("Technologie \"" + technologie.getNazev() + "\" byla úspěšně vytvořena!");
        FacesContext.getCurrentInstance().addMessage("neco", msg);

        return String.format("technologie.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }

    /**
     * upraví zvolenou technologii
     *
     * @return
     */
    public String update() {
        technologieService.update(technologie);
        
        FacesMessage msg = new FacesMessage("Technologie \"" + technologie.getNazev() + "\" byla úspěšně změněna!");
        FacesContext.getCurrentInstance().addMessage("neco", msg);

        return String.format("technologie.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    
    public String back() {
        return "technologie.xhtml?faces-redirect=true";
    }
    
    public void init() {
        if (getTechnologie() == null || (!userService.getCurrentUser().hasRole(UserRole.TUTOR) && !userService.getCurrentUser().hasRole(UserRole.ADMIN))) {
            pageRedirect("/index.xhtml");
        }
    }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.services.TechnologieService;
import java.util.List;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author balikm1
 */
@Component("technologieConverter") 
public class TechnologieConverter implements Converter {
    
    @Autowired
    TechnologieService technologieService;
 
    @Override 
    public String getAsString(FacesContext context, UIComponent component, Object object) { 
        return ((Technologie) object).getId().toString(); 
    } 
 
    @Override 
    public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
        return technologieService.findById(Integer.parseInt(submittedValue));
    }
}
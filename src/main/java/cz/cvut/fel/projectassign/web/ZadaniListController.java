/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.model.Zadani;
import cz.cvut.fel.projectassign.model.ZadaniStatus;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import cz.cvut.fel.projectassign.services.UserService;
import cz.cvut.fel.projectassign.services.ZadaniService;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.ListDataModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně seznamem se
 * zadáními.
 *
 * @author Tom
 */
@Component("zadaniListController")
@Scope("session")
public class ZadaniListController extends AbstractController {

    @Autowired
    private ZadaniService zadaniService;
    @Autowired
    private UserService userService;
    @Autowired
    private ParalelkaService paralelkaService;
    
    private ZadaniDataModelRecord selectedZadaniRecord;
    private Zadani selectedZadani;
    private ListDataModel<ZadaniDataModelRecord> zadaniDataModel;
    private String filterName = "";
    private int filterStatus = 0;
    private int rows = 10;
    private boolean newSearch = true;

    //<editor-fold defaultstate="collapsed" desc="getters and settres">
    public ListDataModel<ZadaniDataModelRecord> getZadaniDataModel() {
        if (zadaniDataModel == null) {
            rebindData();
        }
        return zadaniDataModel;
    }

    public ZadaniDataModelRecord getSelectedZadaniRecord() {
        return selectedZadaniRecord;
    }

    public void setSelectedZadaniRecord(ZadaniDataModelRecord selectedZadani) {
        this.selectedZadaniRecord = selectedZadani;
        this.selectedZadani = null;
    }
    
    public Zadani getSelectedZadani() {
        if (selectedZadaniRecord == null) {
            return null;
        } else {
            if (selectedZadani == null) {
                selectedZadani = zadaniService.findById(getSelectedZadaniRecord().getId());
            }
            return selectedZadani;
        }
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String name) {
        this.filterName = name;
    }

    public int getFilterStatus() {
        return filterStatus;
    }

    public void setFilterStatus(int status) {
        this.filterStatus = status;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="action handlers">
    /**
     * zobrazí editování zadání podle role
     * @return 
     */
    public String edit() {
        if (userService.getCurrentUser().hasRole(UserRole.USER)) {
            return "editZadani.xhtml?id=" + userService.getCurrentUser().getZadani().getId() + "faces-redirect=true";
        } else {

            return "editZadani.xhtml?id=" + getSelectedZadaniRecord().getId() + "&faces-redirect=true";
        }
    }

    public String accept() {
        return "schvalitZadani.xhtml?id=" + getSelectedZadaniRecord().getId() + "&faces-redirect=true";
    }

    public String detail() {
        return "detailZadani.xhtml?id=" + getSelectedZadaniRecord().getId() + "&faces-redirect=true";
    }
/**
 * smaže zvolené zadání
 * 
 * @return 
 */
    public String delete() {
        zadaniService.deleteZadani(getSelectedZadani(), userService.getCurrentUser());
        
        FacesMessage msg = new FacesMessage("Zadani " + getSelectedZadaniRecord().getNazev() + " bylo smazáno!");
        FacesContext.getCurrentInstance().addMessage("neco", msg);
        
        newSearch = true;
        rebindData();
        setSelectedZadaniRecord(null);
        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
/**
 * vyhledá podle filtrů
 * @return 
 */
    public String search() {
        String popis = (filterName == null) ? "" : filterName.trim();
        newSearch = true;
        return "zadani.xhtml?name=" + popis + "&filtr=" + filterStatus + "&faces-redirect=true";
    }
/**
 * rezervuje zvolené zadání
 * @return 
 */
    public String rezervovat() {
        if (tryExecute(new Runnable() {
            @Override
            public void run() {
                zadaniService.rezervovatZadani(getSelectedZadani(), userService.getCurrentUser());
            }
        })) {
            FacesMessage msg = new FacesMessage("Zadani " + getSelectedZadaniRecord().getNazev() + " bylo rezervováno!");
            FacesContext.getCurrentInstance().addMessage("neco", msg);
        }
        
        newSearch = true;
        return String.format("zadani.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="features visibility">
    public boolean acceptDisabled() {
        User user = userService.getCurrentUser();
        return (getSelectedZadaniRecord() == null
                || getSelectedZadaniRecord().getStav() != ZadaniStatus.REQUESTED
                || !(user.hasRole(UserRole.TUTOR) && user.getParalelka().contains(paralelkaService.findByRequestedZadaniId(getSelectedZadaniRecord().getId()))));
    }

    public boolean createDisabled() {
        return zadaniService.createDisabled(userService.getCurrentUser());
    }

    public boolean editDisabled() {
        return zadaniService.editDisabled(getSelectedZadani(), userService.getCurrentUser());
    }

    public boolean detailDisabled() {
        return getSelectedZadaniRecord() == null;
    }

    public boolean deleteDisabled() {
        return !getSelectedZadani().getZadaniStudents().isEmpty();
    }

    public boolean reservationDisabled() {
        return zadaniService.reservationDisabled(getSelectedZadani(), userService.getCurrentUser());

    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="helper methods">
    private void rebindData() {
        List<ZadaniDataModelRecord> zw = new ArrayList<ZadaniDataModelRecord>();
        for (Zadani z : zadaniService.findByFilter(filterName, filterStatus)) {
            zw.add(new ZadaniDataModelRecord(z.getId(), z.getNazev(), getStatusText(z), z.getStav(), !z.getZadaniStudents().isEmpty()));
        }
        zadaniDataModel = new ListDataModel<ZadaniDataModelRecord>(zw);
    }

    public String getStatusText(Zadani zadani) {
        return zadaniService.getStatusText(zadani);
    }

    public void getFilterParameters(String filterName, String filterStatus) {
        if (newSearch) {
            if (filterStatus.equals("")) {
                this.filterStatus = 0;
            } else {
                this.filterStatus = Integer.parseInt(filterStatus);
            }

            this.filterName = filterName;
            newSearch = false;
            rebindData();
        }
    }
    
    public void init() {
        if (isRebindFormDataEnabled())
            rebindData();
    }
    //</editor-fold>
}

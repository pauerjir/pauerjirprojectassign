/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web.notifications;

import cz.cvut.fel.asf.notifications.template.NotificationBase;
import cz.cvut.fel.projectassign.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author balikm1
 */
public class ProjectEvaluatedNotification extends NotificationBase {
    private User user;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    public ProjectEvaluatedNotification(User user) {
        this.user = user;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Interface implementation">
    @Override
    protected String getCode() {
        return "ProjectEvaluated";
    }

    @Override
    protected List<InternetAddress> getRecipients() throws AddressException {
        List<InternetAddress> l = new ArrayList<InternetAddress>();
        l.add(new InternetAddress(user.getEmail()));
        return l;
    }

    @Override
    protected Map<String, String> getReplacements() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("%WEBSITE_ROOTURL%", "https://edux.feld.cvut.cz/ProjectAssignPR1/" /*AbstractController.getRootUrl()*/);
        map.put("%ZADANI_NAME%", (user.getZadani() != null) ? user.getZadani().getNazev() : "");
        map.put("%OHODNOCENI_TEXT%", (user.getHodnoceni() != null) ? user.getHodnoceni().getHodnoceni() : "");
        return map;
    }
    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.asf.web.primefaces.AbstractController;
import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.services.TechnologieService;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně seznamem s
 * technologiemi.
 *
 * @author Tom
 */
@Component("technologieListController")
@Scope("session")
public class TechnologieListController extends AbstractController {

    @Autowired
    private TechnologieService technologieService;
    private Technologie selectedTechnologie;
    private TechnologieDataModel technologieDataModel;
    private String filterName = "";
    private Technologie technologie;
    private int rows = 10;
    private boolean newSearch = true;
    //<editor-fold defaultstate="collapsed" desc="views">

    /**
     * Presune na stránku s vytvářením technologie
     *
     * @return
     */
    public String create() {
        return "createTechnologie.xhtml?faces-redirect=true";
    }

    /**
     * Přesune na stránku pro editaci
     *
     * @return
     */
    public String edit() {
        return "editTechnologie.xhtml?id=" + selectedTechnologie.getId() + "&faces-redirect=true";
    }

    /**
     * smaže zvolenou technologii
     *
     * @return
     */
    public String delete() {
        technologieService.deleteTechnologie(selectedTechnologie);
        rebindData();
        newSearch = true;
        FacesMessage msg = new FacesMessage("Zvolená technologie byla úspěšně smazána!");
        FacesContext.getCurrentInstance().addMessage("neco", msg);
        
        selectedTechnologie = null;
        return "technologie.xhtml?faces-redirect=true";
    }

    /**
     * nastaví vyhledávání
     *
     * @return
     */
    public String search() {
        newSearch = true;
        String popis = (filterName == null) ? "" : filterName.trim();
        return "technologie.xhtml?name=" + popis + "&faces-redirect=true";
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="helper method">
    private void rebindData() {
        technologieDataModel = new TechnologieDataModel(technologieService.getAllTechnologieByName(filterName), technologieService);
    }

    public TechnologieDataModel getTechnologieDataModel() {
        if (technologieDataModel == null) {
            rebindData();
        }
        return technologieDataModel;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="features visibility">
    public boolean edidDisabled() {
        return (selectedTechnologie == null);
    }

    public boolean deleteDisabled() {
        return (selectedTechnologie == null /*|| !technologieService.findById(selectedTechnologie.getId()).getTechnologieZadani().isEmpty()*/);
    }

    public String showMessageDisable() {
        return "true";
//        if (technologieService.isNewMessage()) {
//            return "true";
//        } else {
//            return "";
//        }
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="getters and setters">
    public Technologie getSelectedTechnologie() {
        return selectedTechnologie;
    }

    public void setSelectedTechnologie(Technologie selectedTechnologie) {
        this.selectedTechnologie = selectedTechnologie;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(String name) {
        this.filterName = name;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="inicializacni methody">
    public void findTechnologieById(int id) {
        technologie = technologieService.findById(id);

    }

    /**
     * Zobrazí zpravu nebo zmení obsah seznamu
     *
     * @param filterName
     */
    public void getFilterParameters(String filterName) {
        if (newSearch) {
            newSearch = false;
            this.filterName = filterName;
            rebindData();
        }
      

    }
    //</editor-fold>
    
    public void init() {
        if (isRebindFormDataEnabled())
            rebindData();
    }
}

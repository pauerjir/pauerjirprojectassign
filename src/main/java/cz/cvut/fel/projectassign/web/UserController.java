/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web;

import cz.cvut.fel.projectassign.daos.HodnoceniDAO;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.HodnoceniService;
import cz.cvut.fel.projectassign.services.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * Třída prezentační vrstvy zajištující zobrazeni dat, zpracování vstupů od
 * uživatelů a volá metody servisní vrstvy. Zabývá se prevážně uživateli.
 *
 * @author Tom
 */
@Component("userController")
@Scope("request")
public class UserController implements Serializable {

    @Autowired
    private HodnoceniDAO hodnoceniDAO;
    @Autowired
    private HodnoceniService hodnoceniService;
    @Autowired
    private UserService userService;
    private String heslo;
    private String heslo2;
    private String oldHeslo;
    private User user ;
    
    public static String REBIND_FORM_DATA = "RFD";

    //<editor-fold defaultstate="collapsed" desc="změna hesla">
    /**
     * Zmení heslo zvolenému uživateli
     */
    public void changePassword(User u) {
        PasswordEncoder encoder = new ShaPasswordEncoder();
        u.setPassword(encoder.encodePassword(u.getPassword(), null));
        //service.update(u);
    }

    /**
     * Zmení heslo aktualnímu uživateli
     */
    public void changePassword() {
        PasswordEncoder encoder = new ShaPasswordEncoder();
        if (userService.getCurrentUser().getPassword().equals(encoder.encodePassword(oldHeslo, null)) && heslo.equals(heslo2)) {

            userService.getCurrentUser().setPassword(encoder.encodePassword(heslo, null));
            //service.update(service.getCurrentUser());
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Heslo bylo úspěšně změneno."));
        } else {

            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Špatně zadané původní heslo! "));
        }

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="setters and getters">
    public User getUser() {
        if (user == null) {
            Map<String, String> parameterMap = (Map<String, String>) FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
            String id = parameterMap.get("id");

            if (id != null) {
                try {
                    user = userService.findById(Integer.valueOf(id));
                } catch (NumberFormatException e) {
                    return null;
                }
            }
        }
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getCurrentUser() {
        return userService.getCurrentUser();
    }

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }

    public String getHeslo2() {
        return heslo2;
    }

    public void setHeslo2(String heslo2) {
        this.heslo2 = heslo2;
    }

    public String getOldHeslo() {
        return oldHeslo;
    }

    public void setOldHeslo(String oldHeslo) {
        this.oldHeslo = oldHeslo;
    }
    
//    public Hodnoceni getHodnoceni() {
//        hodnoceniDAO.attach(getUser().getHodnoceni());
//        return getUser().getHodnoceni();
//    }

//    public String getHodnoceni() {
//        return getUser().getHodnoceni().getHodnoceni();
//    }
//    
//    public void setHodnoceni(String hodnoceni) {
//        getUser().getHodnoceni().setHodnoceni(hodnoceni);
//    }
    //</editor-fold>

    
    /**
      * aktualizuje hodnoceni
      * @return 
      */
    public String ohodnoceno(){
//      newSearch=true;
//      rebindData();
        hodnoceniService.update(getUser().getHodnoceni());
        
        FacesMessage msg = new FacesMessage("Zadání bylo ohodnoceno.");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return String.format("userList.xhtml?%s=1&faces-redirect=true", REBIND_FORM_DATA);
     }
    
    public String zpet(){             
      return "userList.xhtml?faces-redirect=true";
     }
    
    public void validateRights() {
        if (getUser() == null || (!getCurrentUser().hasRole(UserRole.TUTOR) && !getCurrentUser().hasRole(UserRole.ADMIN))) {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            try {
                ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
            } catch (IOException ex) {
                throw new RuntimeException("Page not found", ex);
            }
        }
    }
}

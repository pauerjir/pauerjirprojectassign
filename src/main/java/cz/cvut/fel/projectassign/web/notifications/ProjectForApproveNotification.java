/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.web.notifications;

import cz.cvut.fel.asf.notifications.template.NotificationBase;
import cz.cvut.fel.projectassign.daos.UserDAO;
import cz.cvut.fel.projectassign.model.User;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author balikm1
 */
public class ProjectForApproveNotification extends NotificationBase {
    private User student;
    private User tutor;
    
    @Autowired
    UserDAO userDao;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    public ProjectForApproveNotification(User tutor, User student) {
        this.student = student;
        this.tutor = tutor;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Interface implementation">
    @Override
    protected String getCode() {
        return "ProjectForApprove";
    }

    @Override
    protected List<InternetAddress> getRecipients() throws AddressException {
        List<InternetAddress> l = new ArrayList<InternetAddress>();
        l.add(new InternetAddress(tutor.getEmail()));
        return l;
    }

    @Override
    protected Map<String, String> getReplacements() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("%WEBSITE_ROOTURL%", "https://edux.feld.cvut.cz/ProjectAssignPR1/" /*AbstractController.getRootUrl()*/);
        map.put("%STUDENT_NAME%", String.format("%s %s", student.getJmeno(), student.getPrijmeni()));
        map.put("%ZADANI_NAME%", (student.getZadani() != null) ? student.getZadani().getNazev() : "");
        map.put("%ZADANI_TEXT%", (student.getZadani() != null) ? student.getZadani().getPopis() : "");
        return map;
    }
    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "Hodnoceni"
 * @author Tom
 */
@Entity
@Table(name = "Hodnoceni")
@AttributeOverride(name="id", column=@Column(name="id_Hodnoceni"))
public class Hodnoceni extends AbstractPersistable<Integer> {
    
    public static final String NEHODNOCENO = "Zatím nehodnoceno";

    @Column(name = "hodnoceni", columnDefinition="TEXT")
    private String hodnoceni;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_User", insertable = true, updatable = true)
    private User user = null;

    //<editor-fold defaultstate="collapsed" desc="Initialization">
    
    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public Hodnoceni() {
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public Hodnoceni(String hodnoceni, User user) {
        super(true);
        this.hodnoceni = hodnoceni;
        this.user = user;
        user.setHodnoceni(this);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public String getHodnoceni() {
        return hodnoceni;
    }

    public void setHodnoceni(String hodnoceni) {
        this.hodnoceni = hodnoceni;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Associations properties">
    public User getUser() {
        return user;
    }

    protected void setUser(User user) {
        this.user = user;
    }
    //</editor-fold>
}

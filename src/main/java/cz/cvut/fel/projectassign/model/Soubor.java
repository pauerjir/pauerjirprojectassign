package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import java.util.Calendar;
import java.util.Date;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "Soubory"
 *
 * @author Tom
 */
@Entity
@Table(name = "Soubory")
@AttributeOverride(name="id", column=@Column(name="id_Soubor"))
public class Soubor extends AbstractPersistable<Integer> {

    @Column(name = "cesta")
    private String cesta;
    
    @Column(name = "stahnuto")
    private boolean stahnuto;
    
    @Column(name = "datumNahrani")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date datumNahrani;
    
    @Column(name = "datumStazeni")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date datumStazeni;
    
    @Column(name = "puvodniJmeno")
    private String puvodniJmeno;
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "userSoubor", joinColumns = {
        @JoinColumn(name = "id_Soubor")}, inverseJoinColumns = {
        @JoinColumn(name = "id_User")})
    private User user;
    

    //Přidáno git do enumu
    public static enum typSoubor{SEMESTRALKA,DOKUMENTACE,JAVADOC,SQL,ER_DIAGRAM,GIT};
    
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public Soubor() {
    }

    public Soubor(String cesta, String puvodniJmeno) {
        super(true);
        this.cesta = cesta;
        stahnuto = false;
        this.puvodniJmeno = puvodniJmeno;
        datumNahrani = Calendar.getInstance().getTime();

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public String getCesta() {
        return cesta;
    }

    public void setCesta(String cesta) {
        this.cesta = cesta;
    }

    public Date getDatumNahrani() {
        return datumNahrani;
    }

    public void setDatumNahrani(Date datumNahrani) {
        this.datumNahrani = datumNahrani;
    }

    public Date getDatumStazeni() {
        return datumStazeni;
    }

    public void setDatumStazeni(Date datumStazeni) {
        this.datumStazeni = datumStazeni;
    }

    public boolean isStahnuto() {
        return stahnuto;
    }

    public void setStahnuto(boolean stahnuto) {
        this.stahnuto = stahnuto;
    }
    
    public String getPuvodniJmeno() {
        return puvodniJmeno;
    }

    public void setPuvodniJmeno(String puvodniJmeno) {
        this.puvodniJmeno = puvodniJmeno;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Associations properties">
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    //</editor-fold>    
}

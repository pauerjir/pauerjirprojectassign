package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "User"
 *
 * @author Tom
 */
@Entity
@Table(name = "User")
@AttributeOverride(name="id", column=@Column(name="id_User"))
public class User extends AbstractPersistable<Integer> {

    @Column(name = "userName")
    private String userName;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "name")
    private String jmeno;
    
    @Column(name = "surname")
    private String prijmeni;
    
    @OneToMany(mappedBy = "user")
    private Set<UserRole> userRoles = new HashSet<UserRole>();
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "UserParalelka", joinColumns = {
        @JoinColumn(name = "id_User")}, inverseJoinColumns = {
        @JoinColumn(name = "id_Paralelka")})
    private Set<Paralelka> paralelka = new HashSet<Paralelka>();
    
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "Semestralka", joinColumns = {
        @JoinColumn(name = "id_User")}, inverseJoinColumns = {
        @JoinColumn(name = "id_Zadani")})
    private Zadani zadani = null;
    
    @OneToOne(mappedBy = "user")
    private Hodnoceni hodnoceni = null;
    
    @OneToMany(mappedBy = "user")
    private Set<Soubor> soubory = new HashSet<Soubor>();
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public User() {
    }

    public User(String userName, String password, String email) {
        super(true);
        this.userName = userName;
        this.password = password;
        this.email = email;
        jmeno = "";
        prijmeni = "";
    }

    public User(String userName, String password, String email, String jmeno, String prijmeni) {
        super(true);
        this.userName = userName;
        this.password = password;
        this.email = email;
        this.jmeno = jmeno;
        this.prijmeni = prijmeni;
    }

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public String getUserName() {
        return this.userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    
    public String getPassword() {
        return this.password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    
    public String getEmail() {
        return this.email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    
    public String getJmeno() {
        return jmeno;
    }

    public void setJmeno(String jmeno) {
        this.jmeno = jmeno;
    }

    
    public String getPrijmeni() {
        return prijmeni;
    }

    public void setPrijmeni(String prijmeni) {
        this.prijmeni = prijmeni;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Associations properties">
    public Zadani getZadani() {
        return zadani;
    }

    public void setZadani(Zadani zadani) {
        if (this.zadani != null)
            this.zadani.removeStudentInternal(this);
        this.zadani = zadani;
        if (this.zadani != null)
            this.zadani.addStudentInternal(this);
    }
    
    public Hodnoceni getHodnoceni() {
        return hodnoceni;
    }

    void setHodnoceni(Hodnoceni hodnoceni) {
        this.hodnoceni = hodnoceni;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Collections properties">
    public Set<Paralelka> getParalelka() {
        return Collections.unmodifiableSet(paralelka);
    }
    
    public Set<UserRole> getUserRoles() {
        return Collections.unmodifiableSet(userRoles);
    }
    
    public Set<Soubor> getSoubory() {
        return soubory;
    }

    public void setSoubory(Set<Soubor> soubory) {
        this.soubory = soubory;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Process methods">
    public void addParalelka(Paralelka p) {
        p.addStudent(this);
    }

    public void removeParalelka(Paralelka p) {
        p.removeStudent(this);
    }
    
    public void addSoubor(Soubor soubor) {
        soubory.add(soubor);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Internal collection methods">
    void addRoleInternal(UserRole userRole) {
      userRoles.add(userRole);
    }

    void removeRoleInternal(UserRole userRole) {
      userRoles.remove(userRole);
    }
    
    void addParalelkaInternal(Paralelka p) {
      paralelka.add(p);
    }

    void removeParalelkaInternal(Paralelka p) {
      paralelka.remove(p);
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Calculated properties">
    public boolean hasRole(int role) {
        for (UserRole ur : getUserRoles()) {
            if (ur.getIdRole() == role)
                return true;
        }
        return false;
    }
    
    public Paralelka getStudentsParalelka() {
        new BusinessCheckSimpleResult(this.hasRole(UserRole.USER), "User %s %s does not have the role STUDENT.", this.getJmeno(), this.getPrijmeni()).throwEx();
        
        if (this.getParalelka().size() == 1) {
            return (Paralelka)this.getParalelka().toArray()[0];
        } else if (this.getParalelka().isEmpty()) {
            new BusinessCheckSimpleResult(false, "Student %s %s is not assigned to any class.", this.getJmeno(), this.getPrijmeni()).throwEx();
            return null;
        } else {
            throw new RuntimeException("Chyba: Student je přiřazen více paralelkám!");
        }
    }
    //</editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.model;

/**
 * Třída ovladající stavy zadání
 * @author Tom
 */
public class ZadaniStatus {

    public static final byte PUBLIC = 1;
    public static final byte PRIVATE = 2;
    public static final byte NEW = 4;
    public static final byte REQUESTED = 8;
    public static final byte ACCEPTED = 16;

    static public String vratStatus(Zadani z, boolean zparalelky, User user) {

        //  ucitel
        if (!user.hasRole(UserRole.USER)) {
            switch (z.getStav()) {
                case PUBLIC:
                    if (z.getZadaniStudents().isEmpty()) {
                        return "Zadáni je veřejné a žádný student si ho zatím nevybral";
                    } else {
                        return "Zadáni je veřejné a některý student si ho již vybral";
                    }
                case PRIVATE:
                    return "Zadáni je soukromé";
                case NEW:
                    return "Zadáni čeká na studentovo odeslání";
                case REQUESTED:
                    return "!!! Zadáni čeká na schválení učitele !!!";
                case ACCEPTED:
                    return "Připraveno ke schválení studentem";
                default:
                    return "chyba";
            }

        }

        if (user.getZadani() == null || !z.getId().equals(user.getZadani().getId())) {
            switch (z.getStav()) {
                case PUBLIC:
                    if (zparalelky) {
                        return "Obsazené již ho vlastní student z tvojí paralelky";
                    } else {
                        return "Volné zadání" + dovetek(user);
                    }

                case PRIVATE:
                    return "Cizí zadání - schválené";
                default:
                    return "Cizí zadání - rozpracované";

            }
        } else {
            switch (z.getStav()) {
                case PUBLIC:
                    return "Moje zadání";
                case PRIVATE:
                    return "Moje zadání - soukromé";
                case NEW:
                    return "Moje zadání - v editaci";
                case REQUESTED:
                    return "Moje zadání - čeká se na schválení učitele";
                case ACCEPTED:
                    return "Moje zadání - připraveno ke schválení studentem";
                default:
                    return "chyba";
            }
        }

    }

    private static String dovetek(User user) {
        if (user.getZadani() != null) {
            return " - již máš vybrané zadání";
        }
        return "";
    }

    public byte SetPublic(byte stav) {
        stav |= PUBLIC;
        return stav;
    }

    public byte SetPrivate(byte stav) {
        stav |= PRIVATE;
        return stav;
    }

    public byte SetNew(byte stav) {
        stav |= NEW;
        return stav;
    }

    public byte SetRequested(byte stav) {
        stav |= REQUESTED;
        return stav;
    }

    public byte SetAccepted(byte stav) {
        stav |= ACCEPTED;
        return stav;
    }

    public byte RemovePublic(byte stav) {
        stav &= ~PUBLIC;
        return stav;
    }

    public byte RemovePrivate(byte stav) {
        stav &= ~PRIVATE;
        return stav;
    }

    public byte RemoveNew(byte stav) {
        stav &= ~NEW;
        return stav;
    }

    public byte RemoveRequested(byte stav) {
        stav &= ~REQUESTED;
        return stav;
    }

    public byte RemoveAccepted(byte stav) {
        stav &= ~ACCEPTED;
        return stav;
    }
}

package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import java.util.*;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "Zadani"
 * @author Tom
 */
@Entity
@Table(name = "Zadani")
@AttributeOverride(name="id", column=@Column(name="id_Zadani"))
public class Zadani extends AbstractPersistable<Integer> {

    @Column(name = "nazev", length = 100)
    private String nazev;
    
    @Column(name = "popis", columnDefinition="TEXT")
    private String popis;
    
    @Column(name = "stav")
    private int stav;
    
    @OneToMany(mappedBy = "zadani")
    private Set<User> zadaniStudents = new HashSet<User>(0);
    
    @ManyToMany
    @JoinTable(name = "TechnologieZadani", joinColumns = {
        @JoinColumn(name = "id_Zadani")}, inverseJoinColumns = {
        @JoinColumn(name = "id_Technologie")})
    private Set<Technologie> technologies = new HashSet<Technologie>(0);
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public Zadani() {
    }

    public Zadani(String nazev, String popis, int stav) {
        super(true);
        this.nazev = nazev;
        this.popis = popis;
        this.stav = stav;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getPopis() {
        return popis;
    }

    public void setPopis(String popis) {
        this.popis = popis;
    }

    public int getStav() {
        return stav;
    }

    public void setStav(int stav) {
        this.stav = stav;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Collections properties">
    public Set<User> getZadaniStudents() {
        return Collections.unmodifiableSet(zadaniStudents);
    }

//    public void setZadaniStudents(Set<User> zadaniStudents) {
//        this.zadaniStudents = zadaniStudents;
//    }
    
    public Set<Technologie> getTechnologies() {
        return Collections.unmodifiableSet(technologies);
    }

    public void setTechnologies(Set<Technologie> technologies) {
        this.technologies = technologies;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Internal collection methods">
    void addStudentInternal(User student) {
        zadaniStudents.add(student);
    }

    void removeStudentInternal(User student) {
        zadaniStudents.remove(student);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Process methods">
    public void addTechnology(Technologie tech) {
        technologies.add(tech);
    }
    
    public void removeTechnology(Technologie tech) {
        technologies.remove(tech);
    }
    //</editor-fold>
}

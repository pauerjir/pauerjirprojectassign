package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "Technologie"
 *
 * @author Tom
 */
@Entity
@Table(name = "Technologie")
@AttributeOverride(name="id", column=@Column(name="id_Technologie"))
public class Technologie extends AbstractPersistable<Integer> {

    @Column(name = "nazev")
    private String nazev;
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public Technologie() {
    }

    public Technologie(String nazev) {
        super(true);
        this.nazev = nazev;


    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }
    //</editor-fold>
}

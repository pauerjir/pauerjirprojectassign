package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.ICanBeComplexDeleted;
import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "UserRole"
 * @author Tom
 */
@Entity
@Table(name = "UserRole")
@AttributeOverride(name="id", column=@Column(name="id_UserRole"))
public class UserRole extends AbstractPersistable<Integer> implements ICanBeComplexDeleted {

    @Column(name = "id_Role")
    private int idRole;
    
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_User", insertable = true, updatable = true)
    private User user;
    
    public static final int ADMIN = 2;
    public static final int TUTOR = 1;
    public static final int USER = 0;
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public UserRole() {
    }

    @SuppressWarnings("LeakingThisInConstructor")
    public UserRole(int idRole, User user) {
        super(true);
        this.idRole = idRole;
        this.user = user;
        user.addRoleInternal(this);
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public int getIdRole() {
        return this.idRole;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Associations properties">
    protected User getUser() {
        return user;
    }

    protected void setUser(User user) {
        this.user = user;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="ICanBeComplexDeletedMethods">
    @Override
    public void processPreDelete() {
        user.removeRoleInternal(this);
    }
    //</editor-fold>
}

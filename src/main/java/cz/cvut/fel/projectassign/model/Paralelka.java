package cz.cvut.fel.projectassign.model;

import cz.cvut.fel.asf.persistence.hibernate.AbstractPersistable;
import java.util.*;
import javax.persistence.*;

/**
 * Třída mapujíci databázovou tabulku "Paralelka"
 *
 * @author Tom
 */
@Entity
@Table(name = "Paralelka")
@AttributeOverride(name="id", column=@Column(name="id_Paralelka"))
public class Paralelka extends AbstractPersistable<Integer> {

    @Column(name = "cisloParalelky")
    private int cisloParalelky;
    
    @Column(name = "cas")
    private int cas;
    
    @Column(name = "den")
    private int den;
    
    @Column(name = "deadline_vyber")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date deadlineVyber;
    
    @ManyToMany(mappedBy = "paralelka")
    private Set<User> paralelkaStudents = new HashSet<User>(0);
    
    //<editor-fold defaultstate="collapsed" desc="Initialization">

    /**
     * @deprecated This constructor is intended for internal use only, do not use it to instantiate the entity.
     */
    @Deprecated
    public Paralelka() {
    }

    public Paralelka(int cisloParalelky) {
        super(true);
        this.cisloParalelky = cisloParalelky;
        //   ucitel = null;
        cas = 0;
        den = 0;
        deadlineVyber = new GregorianCalendar(2100, Calendar.DECEMBER, 24).getTime();
    }

    public Paralelka(int cisloParalelky, int cas, int den) {
        super(true);
        this.cisloParalelky = cisloParalelky;
        this.cas = cas;
        this.den = den;
        deadlineVyber = new GregorianCalendar(2100, Calendar.DECEMBER, 24).getTime();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Fields properties">
    public int getCisloParalelky() {
        return this.cisloParalelky;
    }

    public void setCisloParalelky(int cisloParalelky) {
        this.cisloParalelky = cisloParalelky;
    }

    public int getCas() {
        return cas;
    }

    public void setCas(int cas) {
        this.cas = cas;
    }

    public int getDen() {
        return den;
    }

    public void setDen(int den) {
        this.den = den;
    }

    public Date getDeadlineVyber() {
        return deadlineVyber;
    }

    public void setDeadlineVyber(Date deadlineVyber) {
        this.deadlineVyber = deadlineVyber;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Collections properties">
    public Set<User> getParalelkaStudents() {
        return paralelkaStudents;
    }

    protected void setParalelkaStudents(Set<User> paralelkaStudents) {
        this.paralelkaStudents = paralelkaStudents;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Process methods">
    public void addStudent(User student) {
        paralelkaStudents.add(student);
        student.addParalelkaInternal(this);
    }
    
    public void removeStudent(User student) {
        paralelkaStudents.remove(student);
        student.removeParalelkaInternal(this);
    }
    //</editor-fold>
}

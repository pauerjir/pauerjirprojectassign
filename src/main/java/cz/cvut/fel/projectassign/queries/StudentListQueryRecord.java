package cz.cvut.fel.projectassign.queries;

import cz.cvut.fel.asf.persistence.IQueryRecord;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import java.util.Date;

/**
 *
 * @author balikm1
 */
public class StudentListQueryRecord implements IQueryRecord {
    
    private Integer id;
    private String userName;
    private String firstName;
    private String lastName;
    private Integer projectTopicId;
    private String projectTopic;
    private Boolean isNew;
    private Date lastUploadDate;
    private Boolean isEvaluated;
    private String circle;

    @Override
    public void initialize(Object[] recordDataArray) {
        id = (Integer)recordDataArray[0];
        userName = recordDataArray[1].toString();
        if (recordDataArray[2] != null) {
            firstName = recordDataArray[2].toString();
        }
        if (recordDataArray[3] != null) {
            lastName = recordDataArray[3].toString();
        }
        if (recordDataArray[4] != null) {
            projectTopicId = (Integer)recordDataArray[4];
        }
        if (recordDataArray[5] != null) {
            projectTopic = recordDataArray[5].toString();
        }
        isNew = (Integer)recordDataArray[6] != 0;
        if (recordDataArray[7] != null) {
            lastUploadDate = (Date)recordDataArray[7];
        }
        if (recordDataArray[8] != null) {
            isEvaluated = (Integer)recordDataArray[8] != 0;
        }
        if (recordDataArray[9] != null) {
            circle = recordDataArray[9].toString();
        }
    }
    
    public Integer getId() {
        return id;
    }

    public String getCircle() {
        return circle;
    }

    public String getFirstName() {
        return firstName;
    }

    public Boolean isEvaluated() {
        return isEvaluated;
    }

    public boolean isNew() {
        return isNew;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getLastUploadDate() {
        return lastUploadDate;
    }
    
    public Integer getProjectTopicId() {
        return projectTopicId;
    }

    public String getProjectTopic() {
        return projectTopic;
    }

    public String getUserName() {
        return userName;
    }
    
    // computed properties
    
    public String getProjectTopicTrimmed() {
        if (projectTopic == null) {
            return "";
        }
        return projectTopic.length() > 15 ? projectTopic.substring(0, 14) + "..." : projectTopic;
    }
    
    public String getIsNewString() {
        return isNew ? "Ano" : "Ne";
    }
    
    public String getLastUploadDateString() {
        return lastUploadDate == null ? "Žádný soubor nenalezen" : lastUploadDate.toString();
    }
    
    public String getIsEvaluatedString() {
        if (isEvaluated == null) {
            return "Chybí záznam v tabulce hodnocení";
        }
        return isEvaluated ? "Ano" : "Ne";
    }
}

package cz.cvut.fel.projectassign.queries;

import cz.cvut.fel.asf.persistence.hibernate.QueryBySQL;
import cz.cvut.fel.asf.tools.StringHelper;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.Paralelka;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author balikm1
 */
public class StudentListQuery extends QueryBySQL<StudentListQueryRecord> {
    
    private String username;
    private List<Paralelka> circles;
    
    public StudentListQuery(String username, List<Paralelka> circles) {
        this.username = username;
        this.circles = circles;
    }

    @Override
    protected String getQueryStringForCount() {
        return String.format("SELECT DISTINCT COUNT(*)"
                + " FROM User u"
                + " JOIN UserRole ur ON ur.id_User = u.id_User"
                + " JOIN UserParalelka up ON up.id_User = u.id_User"
                + " JOIN Paralelka p ON p.id_Paralelka = up.id_Paralelka"
                + " WHERE %s", getWhereExpression());
    }

    @Override
    protected String getQueryStringForList() {
        return String.format("SELECT DISTINCT"
                + " u.id_User,"
                + " u.userName,"
                + " u.name,"
                + " u.surname,"
                + " z.id_Zadani,"
                + " z.nazev,"
                + " (CASE WHEN sou.id_Soubor IS NOT NULL AND sou.datumStazeni IS NULL THEN 1 ELSE 0 END) AS isNew,"
                + " upl.lastUploadDate,"
                + " (CASE WHEN h.hodnoceni = '' OR h.hodnoceni = '%s' THEN 0 ELSE 1 END) AS isEvaluated,"
                + " p.cisloParalelky"
                + " FROM User u"
                + " JOIN UserRole ur ON ur.id_User = u.id_User"
                + " JOIN UserParalelka up ON up.id_User = u.id_User"
                + " JOIN Paralelka p ON p.id_Paralelka = up.id_Paralelka"
                + " LEFT JOIN Semestralka s ON s.id_User = u.id_User"
                + " LEFT JOIN Zadani z ON z.id_Zadani = s.Id_Zadani"
                + " LEFT JOIN Hodnoceni h ON h.id_User = u.id_User"
                + " LEFT JOIN"
                + "  (SELECT"
                + "    u.id_User,"
                + "    MAX(sou.datumNahrani) AS lastUploadDate"
                + "   FROM"
                + "    User u"
                + "   LEFT JOIN userSoubor us ON us.id_User = u.id_User"
                + "   LEFT JOIN Soubory sou ON sou.id_Soubor = us.id_Soubor"
                + "   GROUP BY"
                + "    u.id_User"
                + "  ) upl ON upl.id_User = u.id_User"
                + " LEFT JOIN userSoubor us ON us.id_User = u.id_User"
                + " LEFT JOIN Soubory sou ON sou.id_Soubor = us.id_Soubor"
                + " WHERE %s", Hodnoceni.NEHODNOCENO, getWhereExpression());
    }

    @Override
    protected String getDefaultSortExpression() {
        return "userName";
    }

    @Override
    protected void setQueryParameters(PreparedStatement statement, QueryExecutionPurpose purpose) {
        int i = 1;
        try {
            if (!StringHelper.isNullOrEmpty(username)) {
                statement.setString(i++, String.format("%s%%", username));
            }
        } catch (SQLException ex) {
            throw new IllegalArgumentException("Setting query parameters failed.", ex);
        }
    }

    @Override
    protected void initializePropertiesDictionary() {
        getPropertiesDictionary().put("circle", "p.cisloParalelky");
    }
    
    private String getWhereExpression() {
        String expr = "1 = 1";
        expr += " AND ur.Id_Role = 0";
        
        if (!StringHelper.isNullOrEmpty(username)) {
            expr += " AND u.userName LIKE ?";
        }
        
        if (circles != null && !circles.isEmpty()) {
            expr += String.format(" AND p.cisloparalelky IN (%s)", getComaJoinedCircleList(circles));
        }
        
        return expr;
    }
    
    private String getComaJoinedCircleList(List<Paralelka> circles) {
        List<String> items = new ArrayList<String>();
        for (Paralelka p : circles) {
            items.add(Integer.toString(p.getCisloParalelky()));
        }
        return StringHelper.join(items, ", ");
    }
}

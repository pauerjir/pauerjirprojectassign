package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import java.util.List;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se uživateli
 * @author Tom
 */
public interface UserService {

    public List<User> getAllUsers();

    public User create(String userName, String heslo, String email, String jmeno, String prijmeni);

    public User getUserByLogin(String loginName);

    //public void update(User u);

    public User getCurrentUser();

    public User findById(int parseInt);

    public List<User> getAllStudentsByFilter(String search, String filterStatus);
}
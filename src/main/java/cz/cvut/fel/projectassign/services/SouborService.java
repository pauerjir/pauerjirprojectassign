package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Soubor;
import cz.cvut.fel.projectassign.model.User;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se Soubory
 * @author Tom
 */
public interface SouborService {

    public Soubor create(String cesta, String puvod);

    //public void update(Soubor soubor);

    public Soubor findByCesta(String cesta);

    public void uploadSemestralky(FileUploadEvent event, User user,Soubor.typSoubor typ);

    public String parseFileName(String fileName, Soubor.typSoubor typ);

    public StreamedContent zipAllNewestFilesByType(User user);

    public StreamedContent downloadFile(File file) throws FileNotFoundException;

    public File[] getAllFiles(User user);

    public File[] getFilesNew(User user);

    public File getFileNew(User user, String pripona);

    public List<File> getFileList(User user);

    public void oznacitStazene(User user);

    public boolean downloadDisable(User user);
}
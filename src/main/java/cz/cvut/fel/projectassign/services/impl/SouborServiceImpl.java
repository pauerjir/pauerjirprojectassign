package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.daos.impl.*;
import cz.cvut.fel.projectassign.model.*;
import cz.cvut.fel.projectassign.services.SouborService;
import cz.cvut.fel.projectassign.web.UploadController;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o soubory
 * pro download a upload
 *
 * @author Tom
 */
@Transactional
@Component("souborService")
public class SouborServiceImpl implements SouborService {

    @Autowired
    private SouborDAOImpl daoSoubor;
    @Autowired
    private UserDAOImpl daoUser;
    @Autowired
    private ParalelkaDAOImpl daoParalelka;
    @Autowired
    private HodnoceniDAOImpl daoHodnoceni;
    @Autowired
    private UserRoleDAOImpl daoUserRole;
    private List<File> fileList;
    private StreamedContent sFile;

    @Override
    public Soubor create(String cesta, String puvod) {
        return daoSoubor.create(cesta, puvod);
    }

//    @Override
//    public void update(Soubor soubor) {
//        daoSoubor.save(soubor);
//    }

    @Override
    public Soubor findByCesta(String cesta) {
        return daoSoubor.findByCesta(cesta);
    }

    //<editor-fold defaultstate="collapsed" desc="upload">
    /**
     * nahraje soubor zvoleneho uzivatele
     *
     * @param event
     * @param user
     */
    @Override
    public void uploadSemestralky(FileUploadEvent event, User user,Soubor.typSoubor typ) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-hh-mm");

        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        new File(sc.getRealPath("/semestralky/" + user.getUserName())).mkdirs();

        File result = new File(sc.getRealPath("/semestralky/" + user.getUserName() + "/" + dateFormat.format(calendar.getTime()).toString().trim() + "__" + user.getUserName() + "__" + parseFileName(event.getFile().getFileName(),typ)));

        try {
            FileOutputStream fileOutputStream = new FileOutputStream(result);

            byte[] buffer = new byte[128];

            int bulk;
            InputStream inputStream = event.getFile().getInputstream();
            while (true) {
                bulk = inputStream.read(buffer);
                if (bulk < 0) {
                    break;
                }
                fileOutputStream.write(buffer, 0, bulk);
                fileOutputStream.flush();
            }

            fileOutputStream.close();
            inputStream.close();

            Soubor soubor = daoSoubor.create("/semestralky/" + user.getUserName() + "/" + dateFormat.format(calendar.getTime()).toString().trim() + "__" + user.getUserName() + "__" + parseFileName(event.getFile().getFileName(),typ), event.getFile().getFileName());
            soubor.setUser(user);
            user.addSoubor(soubor);

            //daoUser.save(user);
            //daoSoubor.save(soubor);


            FacesMessage msg = new FacesMessage("Povedlo se:", "Soubor "
                    + event.getFile().getFileName() + " byl úspěšně náhrán.");
            FacesContext.getCurrentInstance().addMessage("neco", msg);


        } catch (IOException e) {
            e.printStackTrace();
            FacesMessage error = new FacesMessage("Soubor nebyl nahrán!");
            FacesContext.getCurrentInstance().addMessage(null, error);
        }
    }

    @Override
    public String parseFileName(String fileName, Soubor.typSoubor typ) {
        if (typ.equals(Soubor.typSoubor.SQL)) {
            return ".sql";
        }
        if (typ.equals(Soubor.typSoubor.JAVADOC)) {
            return "jd.zip";
        }
        if (typ.equals(Soubor.typSoubor.SEMESTRALKA)) {
            return "sp.zip";
        }
        if (typ.equals(Soubor.typSoubor.DOKUMENTACE)) {
            return ".pdf";
        }
        if (typ.equals(Soubor.typSoubor.ER_DIAGRAM)) {
            return ".jpg";
        }

        return "chyba";
    }
    
    //</editor-fold> 
    //<editor-fold defaultstate="collapsed" desc="download">
    /**
     * vratí zip archiv souboru uzivatele
     *
     * @param user
     * @return
     */
    @Override
    public StreamedContent zipAllNewestFilesByType(User user) {
        File[] source = getFilesNew(user);
        byte[] buf = new byte[1024];

        try {
            // Create the ZIP file
            ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
            File target = new File(sc.getRealPath("/semestralky/" + user.getUserName() + ".zip"));
            // FileOutputStream zipFile = new FileOutputStream(target);

            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(target));

            // Compress the files
            for (int i = 0; i < source.length; i++) {
                //   FileInputStream in = new FileInputStream(source[i].getPath());
                FileInputStream in = new FileInputStream(sc.getRealPath("/semestralky/" + user.getUserName() + "/" + source[i].getName()));

                // Add ZIP entry to output stream.
                out.putNextEntry(new ZipEntry(source[i].getName()));

                // Transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }

                // Complete the entry
                out.closeEntry();
                in.close();
            }

            // Complete the ZIP file
            out.close();

            InputStream stream = new FileInputStream(target);
            sFile = new DefaultStreamedContent(stream, target.getAbsolutePath(), target.getName());
            // target.delete();

        } catch (IOException e) {
            e.printStackTrace();
        }
        oznacitStazene(user);

        return sFile;
    }

    /**
     * vrati zvoleny soubor pripraven ke stažení
     *
     * @param file
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public StreamedContent downloadFile(File file) throws FileNotFoundException {
        InputStream stream;
        stream = new FileInputStream(file);

        sFile = new DefaultStreamedContent(stream, file.getAbsolutePath(), file.getName());
        return sFile;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="get all and new files">
    /**
     * vrátí všechny soubory zvoleného uživatele
     *
     * @param user
     * @return
     */
    @Override
    public File[] getAllFiles(User user) {
        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        File folder = new File(sc.getRealPath("/semestralky/" + user.getUserName()));
        File[] listOfFiles = folder.listFiles();

        if (listOfFiles == null) {
            return null;
        } else {
            return listOfFiles;
        }
    }

    /**
     * vráti z kazde kategorie nejnovejsi soubor
     *
     * @param user
     * @return
     */
    @Override
    public File[] getFilesNew(User user) {
        List<File> allnew = new ArrayList<File>();
        File f;
        if ((f = getFileNew(user, "sp.zip")) != null) {
            allnew.add(f);
        }
        if ((f = getFileNew(user, "jd.zip")) != null) {
            allnew.add(f);
        }
        if ((f = getFileNew(user, ".sql")) != null) {
            allnew.add(f);
        }
        if ((f = getFileNew(user, ".jpg")) != null) {
            allnew.add(f);
        }
        if ((f = getFileNew(user, ".pdf")) != null) {
            allnew.add(f);
        }

        File[] newFiles = (File[]) allnew.toArray(new File[allnew.size()]);

        if (newFiles == null) {
            return null;
        } else {
            return newFiles;
        }
    }

    /**
     * podle pripony vrati nejnovejsi soubor
     *
     * @param user
     * @param pripona
     * @return
     */
    @Override
    public File getFileNew(User user, String pripona) {
        fileList = getFileList(user);
        File file = null;
        Iterator<File> iter = fileList.iterator();
        File f;
        long nejmladsiSoubor = 0;
        while (iter.hasNext()) {
            f = iter.next();
            if (f.getName().endsWith(pripona) && f.lastModified() > nejmladsiSoubor) {
                nejmladsiSoubor = f.lastModified();
                file = f;
            }
        }
        return file;
    }

    /**
     * vrátí všechny soubory uzivatele
     *
     * @param user
     * @return
     */
    @Override
    public List<File> getFileList(User user) {
        ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        File folder = new File(sc.getRealPath("/semestralky/" + user.getUserName()));
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles == null) {
            fileList = new ArrayList<File>();
        } else {
            fileList = new ArrayList<File>();
            fileList.addAll(Arrays.asList(listOfFiles));
        }
        return fileList;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="pomocne metody">
    /**
     * Vsechny dosud nestažené soubory oznaci jako stazené
     *
     * @param user
     */
    @Override
    public void oznacitStazene(User user) {
        User u = daoUser.findById(user.getId());
        if (u.getSoubory() != null) {
            Iterator<Soubor> iter = u.getSoubory().iterator();

            while (iter.hasNext()) {
                Soubor s = iter.next();
                s.setStahnuto(true);
                if (s.getDatumStazeni()==null || s.getDatumStazeni().getTime() < 10000) {
                    s.setDatumStazeni(Calendar.getInstance().getTime());
                }
                //daoSoubor.save(s);
            }
        }
    }
    //</editor-fold>

    

    public void zalohovat() {
        Iterator<User> iter= daoUser.findAll().iterator();
        User user;
        while(iter.hasNext()){
        user=iter.next();
        if(!user.hasRole(UserRole.USER))continue;
        
          ServletContext sc = (ServletContext) FacesContext.getCurrentInstance().getExternalContext().getContext();
        new File(sc.getRealPath("/semestralky/"+ user.getUserName())).mkdirs();

        File result = new File(sc.getRealPath("/semestralky/" +"/"+ user.getUserName() + "/" + "Zadani&Hodnoceni.txt"));
            try {
                FileWriter fw = new FileWriter(result);
            

                if(user.getZadani()!=null){
                    fw.write(user.getZadani().toString());
                }else{
                    fw.write("Student si nezvolil žádné zadání");
                }
               fw.write("\r\n\r\nHodnocení:\r\n\r\n");
               fw.write(user.getHodnoceni().getHodnoceni());
               fw.close();
            
            } catch (IOException ex) {
                Logger.getLogger(SouborServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        
        }
        
         FacesMessage error = new FacesMessage("Záloha byla vytvořena");
            FacesContext.getCurrentInstance().addMessage(null, error);
        
        
        
    }
    
    
    //<editor-fold defaultstate="collapsed" desc="visibility">
    @Override
    public boolean downloadDisable(User user) {
        if (getAllFiles(user) == null) {
            return true;
        } else {
            return false;
        }

    }
    //</editor-fold>
}
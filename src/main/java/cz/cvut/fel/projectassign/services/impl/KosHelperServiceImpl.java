/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckCompositeResult;
import cz.cvut.fel.asf.businesschecks.BusinessCheckException;
import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.notifications.template.NotificationSender;
import cz.cvut.fel.asf.tools.StringHelper;
import cz.cvut.fel.projectassign.daos.HodnoceniDAO;
import cz.cvut.fel.projectassign.daos.ParalelkaDAO;
import cz.cvut.fel.projectassign.daos.UserDAO;
import cz.cvut.fel.projectassign.daos.UserRoleDAO;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.KosHelperService;
import cz.cvut.fel.projectassign.security.UserAuthenticationProvider;
import cz.cvut.fel.projectassign.web.notifications.StudentCreatedNotification;
import cz.cvut.fel.projectassign.web.notifications.TutorCreatedNotification;
import java.io.*;
import java.util.AbstractMap.SimpleEntry;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.util.Assert;

/**
 *
 * @author balikm1
 */
@Component("kosHelperService")
public class KosHelperServiceImpl implements KosHelperService {
    
    @Autowired
    private UserDAO userDao;
    
    @Autowired
    private ParalelkaDAO paralelkaDao;
    
    @Autowired
    private UserRoleDAO userRoleDao;
    
    @Autowired
    private HodnoceniDAO hodnoceniDao;
    
    @Autowired
    private NotificationSender notificationSender;
    
    @Autowired
    private TransactionTemplate transactionTemplate;
    
    @Autowired
    UserAuthenticationProvider authProvider;
    
    private int usersCreated;
    
    @Override
    public int getUsersCreated() {
        return usersCreated;
    }
    
    /**
     * parsuje kos file a vytvoří z nej uzivatele, role, hodnoceni, paralelky
     * @param s 
     */
    @Override
    public synchronized void parseKosFile(InputStream s) {
        Assert.notNull(s, "Kos file stream must not be null");
        
        usersCreated = 0;
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(s, "UTF8"));
            
            String line;
            HashMap<String, Integer> userCourseMap = new HashMap<String, Integer>();
            BusinessCheckCompositeResult result = new BusinessCheckCompositeResult();
            while((line = reader.readLine()) != null) {
                line = line.trim();
                if (!line.equals("")) {
                    String[] parts = line.split(":");
                    try {
                        if (parts[0].equals("C")) {
                            SimpleEntry<String, Integer> entry = getUserEntry(parts);
                            userCourseMap.put(entry.getKey(), entry.getValue());
                        } else if (parts[0].equals("P")) {
                            // not needed
                        } else if (parts[0].equals("lector")) {
                            // not needed
                        } else if (parts[0].equals("student")) {
                            storeStudent(parts, userCourseMap);
                        } else if (parts[0].equals("tutor")) {
                            storeTutor(parts);
                        } else {
                            result.addResult(false, "Input line has wrong format: %s", line);
                        }
                    } catch (BusinessCheckException bex) {
                        result.addResult(bex.getResult());
                    }
                }
            }
            result.throwEx();
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(KosHelperServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new IllegalArgumentException("Unsupported KOS data encoding", ex);
        } catch (IOException ex) {
            Logger.getLogger(KosHelperServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Error reading KOS data", ex);
        }
    }
    
    private SimpleEntry<String, Integer> getUserEntry(String[] lineParts) {
        if (lineParts.length < 3)
            new BusinessCheckSimpleResult(false, "Username and number required: %s", StringHelper.join(lineParts, ":")).throwEx();
        
        Integer number = parseNumber(lineParts, 2);
        
        return new SimpleEntry<String, Integer>(lineParts[1], number);
    }
    
    private void storeStudent(final String[] lineParts, final Map<String, Integer> userCourseMap) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            // the code in this method executes in a transactional context
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                new BusinessCheckSimpleResult(lineParts.length >= 4, "Student data not complete: %s", StringHelper.join(lineParts, ":")).throwEx();
                String[] nameParts = lineParts[2].split(" ", 2);
                new BusinessCheckSimpleResult(nameParts.length == 2, "Student must have name and surname: %s", lineParts[2]).throwEx();

                String password = generateRandomPassword();
                User student = userDao.create(lineParts[1], encodePassword(password), String.format("%s@fel.cvut.cz", lineParts[1]), nameParts[0], nameParts[1]);
                userRoleDao.create(student, UserRole.USER);

                new BusinessCheckSimpleResult(userCourseMap.containsKey(student.getUserName()), "Student %s not assigned to any course", student.getUserName()).throwEx();
                Integer courseNumber = userCourseMap.get(student.getUserName());
                Paralelka p = paralelkaDao.findByNumber(courseNumber);
                if (p == null)
                    p = paralelkaDao.create(courseNumber);
                student.addParalelka(p);

                hodnoceniDao.create(Hodnoceni.NEHODNOCENO, student);

                notificationSender.send(new StudentCreatedNotification(student, p, password, authProvider.getUseDbPasswd()));
                usersCreated++;
            }
        });
    }
    
    private void storeTutor(final String[] lineParts) {
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            // the code in this method executes in a transactional context
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                new BusinessCheckSimpleResult(lineParts.length >= 7, "Tutor data not complete: %s", StringHelper.join(lineParts, ":")).throwEx();
                String[] nameParts = lineParts[3].split(" ", 3);
                new BusinessCheckSimpleResult(nameParts.length == 3, "Tutor must have title, name and surname: %s", lineParts[3]).throwEx();

                boolean isNew = false;
                User tutor = userDao.findByLogin(lineParts[1]);
                String password = null;
                if (tutor == null) {
                    isNew = true;
                    password = generateRandomPassword();
                    tutor = userDao.create(lineParts[1], encodePassword(password), String.format("%s@fel.cvut.cz", lineParts[1]), nameParts[1], nameParts[2]);
                    userRoleDao.create(tutor, UserRole.TUTOR);
                }
                if (!tutor.hasRole(UserRole.TUTOR)) {
                    userRoleDao.create(tutor, UserRole.TUTOR);
                }

                Integer courseNumber = parseNumber(lineParts, 2);
                Integer day = parseNumber(lineParts, 4);
                Integer time = parseNumber(lineParts, 5); 

                Paralelka p = paralelkaDao.findByNumber(courseNumber);
                if (p == null) {
                    p = paralelkaDao.create(courseNumber, day, time);
                } else {
                    p.setDen(day);
                    p.setCas(time);
                }
                tutor.addParalelka(p);

                if (isNew) {
                    notificationSender.send(new TutorCreatedNotification(tutor, password, authProvider.getUseDbPasswd()));
                    usersCreated++;
                }
            }
        });
    }
    
    private Integer parseNumber(String[] parts, int numberIndex) {
        Integer number = null;
        try {
            number = Integer.valueOf(parts[numberIndex]);
        } catch (NumberFormatException e) {
            new BusinessCheckSimpleResult(false, "Value '%s' is not number: %s", parts[numberIndex], StringHelper.join(parts, ":")).throwEx();
        }
        return number;
    }
    
    private String generateRandomPassword() {
        String heslo = "heslo";
        return heslo;
    }
    
    private String encodePassword(String password) {
        PasswordEncoder encoder = new ShaPasswordEncoder();
        return encoder.encodePassword(password, null);
    } 
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services;

import org.springframework.security.core.userdetails.UserDetails;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se prihlasovacím procesem
 * @author Tom
 */
public interface MyUserDetailsService {


    public UserDetails loadUserByUsername(String s) ;
}
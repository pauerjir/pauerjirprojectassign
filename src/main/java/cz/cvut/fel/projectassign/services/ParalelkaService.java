package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Paralelka;
import java.util.List;
import java.util.SortedMap;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se paralelkama
 * @author Tom
 */
public interface ParalelkaService {

    public Paralelka create(int cislo, int cas, int den);

    public Paralelka create(int cislo);

    public Paralelka getByClassNumber(int i);

    public void update(Paralelka par);

    public List<Paralelka> getAllParalelka();

    public Paralelka findById(int parseInt);
    
    public Paralelka findByRequestedZadaniId(int zadaniId);

    public String getDayString(Paralelka paralelka);

    public String getCasString(Paralelka paralelka);

    public SortedMap<String, String> setMapParalelky();
}
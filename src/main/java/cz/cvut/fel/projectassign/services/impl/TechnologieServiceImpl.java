/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.daos.impl.TechnologieDaoImpl;
import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.services.TechnologieService;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o
 * technologie
 *
 * @author Tom
 */
@Transactional
@Component("technologieService")
public class TechnologieServiceImpl implements TechnologieService {

    @Autowired
    private TechnologieDaoImpl daoTechnologie;


    //<editor-fold defaultstate="collapsed" desc="dotazy">
    @Override
    public Technologie createTechnologie(String nazev) {
        return daoTechnologie.create(nazev);
    }

    @Override
    public List<Technologie> getAllTechnologie() {
        return daoTechnologie.findAllByName("");
    }

    @Override
    public List<Technologie> getAllTechnologieByName(String search) {
        return daoTechnologie.findAllByName(search);
    }

    @Override
    public void deleteTechnologie(Technologie tech) {
        daoTechnologie.delete(tech);
    }

    @Override
    public void update(Technologie tech) {
//        daoTechnologie.save(tech);
    }

    @Override
    public Technologie findById(int id) {
        return daoTechnologie.findById(id);
    }

    @Override
    public Technologie findByName(String s) {
        return daoTechnologie.findByName(s);
    }
    //</editor-fold>

  
}
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Technologie;
import java.util.List;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se technologiemi
 * @author Tom
 */
public interface TechnologieService {

    public Technologie createTechnologie(String nazev);

    public List<Technologie> getAllTechnologie();

    public List<Technologie> getAllTechnologieByName(String search);

    public void deleteTechnologie(Technologie z);

    public void update(Technologie z);

    public Technologie findById(int id);

    public Technologie findByName(String s);


}
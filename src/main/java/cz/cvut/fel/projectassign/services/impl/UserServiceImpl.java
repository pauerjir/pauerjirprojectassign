package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.projectassign.daos.impl.UserDAOImpl;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.UserService;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o uživatele
 * @author Tom
 */
@Transactional
@Component("userService")
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAOImpl dao;

    @Override
    public List<User> getAllUsers() {

        return dao.findAllByName("");
    }

    @Override
    public User create(String userName, String heslo, String email, String jmeno, String prijmeni) {
        PasswordEncoder encoder = new ShaPasswordEncoder();
        heslo = (encoder.encodePassword(heslo, null));
        return dao.create(userName, heslo, email, jmeno, prijmeni);
    }

    @Override
    public User getUserByLogin(String loginName) {
        return dao.findByLogin(loginName);
    }

//    @Override
//    public void update(User u) {
//        dao.save(u);
//    }

    @Override
    public User getCurrentUser() {
        String loginName = SecurityContextHolder.getContext().getAuthentication().getName();

        return dao.findByLogin(loginName);
    }

    @Override
    public User findById(int parseInt) {
        return dao.findById(parseInt);
    }

    /**
     * vrátí studenty podle zvolenych filtrů na jméno a čislo paralelky
     * @param search
     * @param filterStatus
     * @return 
     */
    @Override
    public List<User> getAllStudentsByFilter(String search, String filterStatus) {
        List<User> userList = dao.findAllByName(search);
        List<User> result = new ArrayList();
        User akt;
        Iterator<User> iter = userList.iterator();
        while (iter.hasNext()) {
            akt = iter.next();
            if (akt.hasRole(UserRole.USER) && (filterStatus == null || filterStatus.equals("0") || filterStatus.equals(""))) {
                result.add(akt);
            }
            if (akt.hasRole(UserRole.USER) && !(filterStatus == null || filterStatus.equals("0") || filterStatus.equals("")) && (akt.getStudentsParalelka().getCisloParalelky() == (Integer.valueOf(filterStatus)))) {
                result.add(akt);
            }
        }

        return result;
    }
}
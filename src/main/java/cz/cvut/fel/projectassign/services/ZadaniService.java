/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.model.Technologie;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.Zadani;
import java.util.List;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se zadáními
 * @author Tom
 */
public interface ZadaniService {

    public Zadani createByUser(String nazev, String popis, int stav, User user, List<Technologie> technologie);

    public List<Zadani> getAllZadani();

    public void deleteZadani(Zadani z, User u);

    public void rezervovatZadani(Zadani z, User user);

    public List<Zadani> findByFilter(String nazev, int stav);

    public Zadani findById(int id);

    public String getStatusText(Zadani z);

    public Zadani findIdenticalZadaniByParalelka(Zadani selectedZadani, Paralelka findParalelkaByUser);

    public void deleteSemestralka(User user);

    public Zadani create(String nazev, String popis, int stav);
    
    public void updateStatus(Zadani z, int status);

    //<editor-fold defaultstate="collapsed" desc="visibility">
    public boolean createDisabled(User user);

    public boolean editDisabled(Zadani zadani, User user);

    public boolean reservationDisabled(Zadani zadani, User user);

    public boolean potvrditDisable(Zadani zadani, User user);

    public boolean deleteSemestralkaDisabled(Zadani zadani, User user);

    public boolean odevzdatDisable(User user);
    //</editor-fold>

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services;

import java.io.InputStream;

/**
 *
 * @author balikm1
 */
public interface KosHelperService {
    int getUsersCreated();
    void parseKosFile(InputStream s);
}

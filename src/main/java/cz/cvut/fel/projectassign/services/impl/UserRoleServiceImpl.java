package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.daos.impl.UserRoleDAOImpl;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import cz.cvut.fel.projectassign.services.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o uživatelské role
 * @author Tom
 */
@Transactional
@Component("userRoleService")
public class UserRoleServiceImpl implements UserRoleService {

    @Autowired
    private UserRoleDAOImpl dao;

    @Override
    public UserRole create(User user, int role) {
        return dao.create(user, role);
    }

//    @Override
//    public void update(UserRole ur) {
//        dao.save(ur);
//    }
}
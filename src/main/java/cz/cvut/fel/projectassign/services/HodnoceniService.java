package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se Hodnocením
 * @author Tom
 */
public interface HodnoceniService {

    public Hodnoceni create(String komentar, User user);

    public Hodnoceni findById(int id);

    public void update(Hodnoceni hodnoceni);
}
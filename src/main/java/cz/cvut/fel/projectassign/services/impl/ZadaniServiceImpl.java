/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.asf.businesschecks.BusinessCheckSimpleResult;
import cz.cvut.fel.asf.notifications.template.NotificationSender;
import cz.cvut.fel.projectassign.daos.impl.TechnologieDaoImpl;
import cz.cvut.fel.projectassign.daos.impl.UserDAOImpl;
import cz.cvut.fel.projectassign.daos.impl.ZadaniDaoImpl;
import cz.cvut.fel.projectassign.model.*;
import cz.cvut.fel.projectassign.services.UserService;
import cz.cvut.fel.projectassign.services.ZadaniService;
import cz.cvut.fel.projectassign.web.notifications.*;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o zadání
 * @author Tom
 */
@Transactional
@Component("zadaniService")
public class ZadaniServiceImpl implements ZadaniService {

    @Autowired
    private ZadaniDaoImpl daoZadani;
    
    @Autowired
    private UserDAOImpl daoUser;
    
    @Autowired
    private TechnologieDaoImpl daoTech;
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private NotificationSender notificationSender;

    @Override
    public Zadani createByUser(String nazev, String popis, int stav, User user, List<Technologie> allTechnologie) {
        Zadani z = daoZadani.create(nazev, popis, stav);
        z.setTechnologies(new HashSet<Technologie>(allTechnologie));

        if (user.hasRole(UserRole.USER)) {
            user.setZadani(z);
        }
        
        if (stav == ZadaniStatus.REQUESTED && user.hasRole(UserRole.USER)) {
            notificationSender.send(new ProjectCreatedNotification(user));
            User tutor = daoUser.findTutorByParalelka(user.getStudentsParalelka());
            if (tutor != null)
                notificationSender.send(new ProjectForApproveNotification(tutor, user));
            else
                Logger.getLogger(ZadaniServiceImpl.class.getName()).log(Level.SEVERE, "Tutor not found for user{0}", userService.getCurrentUser().getUserName());
        }
        
        return z;
    }

    @Override
    public List<Zadani> getAllZadani() {
        return daoZadani.findAll();
    }

    @Override
    public void deleteZadani(Zadani z, User u) {
        daoZadani.delete(z);
        if (u.hasRole(UserRole.USER)) {
            notificationSender.send(new ProjectCanceledNotification(u, z));
        }
    }

    @Override
    public void rezervovatZadani(Zadani z, User user) {
        new BusinessCheckSimpleResult(!reservationDisabled(z, user), "Zvolené zadání není možné rezervovat.").throwEx();
        
        user.setZadani(z);
        
        notificationSender.send(new ProjectChoosedNotification(user));
    }

    @Override
    public List<Zadani> findByFilter(String nazev, int stav) {
        if (stav == 1) {
            return daoZadani.findAllByNameStav(nazev, "2");
        } else {
            return daoZadani.findAllByNameStav(nazev, String.valueOf(Integer.MAX_VALUE));
        }
    }

    @Override
    public Zadani findById(int id) {
        return daoZadani.findById(id);
    }

    @Override
    public String getStatusText(Zadani z) {
        User user = userService.getCurrentUser();
        if (user.hasRole(UserRole.USER) && user.getParalelka() != null && findIdenticalZadaniByParalelka(z, user.getStudentsParalelka()) != null) {
            return ZadaniStatus.vratStatus(z, true, user);
        } else {
            return ZadaniStatus.vratStatus(z, false, user);
        }
    }

    @Override
    public Zadani findIdenticalZadaniByParalelka(Zadani selectedZadani, Paralelka findParalelkaByUser) {
        return daoZadani.findIdenticalZadaniByParalelka(selectedZadani, findParalelkaByUser);
    }

    @Override
    public Zadani create(String nazev, String popis, int stav) {
        return daoZadani.create(nazev, popis, stav);
    }

    @Override
    public void deleteSemestralka(User user) {
        Zadani z = user.getZadani();
        user.setZadani(null);
        notificationSender.send(new ProjectCanceledNotification(user, z));
    }
    
    @Override
    public void updateStatus(Zadani zadani, int stav) {
        boolean isRejected = zadani.getStav() == ZadaniStatus.REQUESTED && stav == ZadaniStatus.NEW;
        zadani.setStav(stav);
        
        if (stav == ZadaniStatus.REQUESTED && userService.getCurrentUser().hasRole(UserRole.USER)) {
            notificationSender.send(new ProjectCreatedNotification(userService.getCurrentUser()));
            User tutor = daoUser.findTutorByParalelka(userService.getCurrentUser().getStudentsParalelka());
            if (tutor != null)
                notificationSender.send(new ProjectForApproveNotification(tutor, userService.getCurrentUser()));
            else
                Logger.getLogger(ZadaniServiceImpl.class.getName()).log(Level.SEVERE, "Tutor not found for user{0}", userService.getCurrentUser().getUserName());
        } else if (isRejected) {
            notificationSender.send(new ProjectInspectedNegativeNotification((User)zadani.getZadaniStudents().toArray()[0]));
        } else if (stav == ZadaniStatus.ACCEPTED) {
            notificationSender.send(new ProjectInspectedPositiveNotification((User)zadani.getZadaniStudents().toArray()[0]));
        }
    }

    //<editor-fold defaultstate="collapsed" desc="visibility">
    @Override
    public boolean createDisabled(User user) {
        return (user.getZadani() != null) || (user.hasRole(UserRole.USER) && ((user.getStudentsParalelka().getDeadlineVyber().getTime()) < (Calendar.getInstance().getTime().getTime())));

    }

    @Override
    public boolean editDisabled(Zadani zadani, User user) {
        if (zadani == null || ((user.getZadani() == null) & (user.hasRole(UserRole.USER)))) {
            return true;
        }
        if (!user.hasRole(UserRole.USER)) {
            return false;
        }

        return !(user.getZadani().equals(zadani) && zadani.getStav() == ZadaniStatus.NEW);
    }

    @Override
    public boolean reservationDisabled(Zadani zadani, User user) {

        if (zadani == null || !user.hasRole(UserRole.USER) || user.getZadani() != null || zadani.getStav() != ZadaniStatus.PUBLIC || (user.getStudentsParalelka().getDeadlineVyber().getTime()) < (Calendar.getInstance().getTime().getTime())) {
            return true;
        }
        //zjisti jestli nekdo z jeho paralelky uz nevlastni toto zadani
        return findIdenticalZadaniByParalelka(zadani, user.getStudentsParalelka()) != null;


    }

    @Override
    public boolean potvrditDisable(Zadani zadani, User user) {
        return (zadani == null || user.getZadani() == null || !(zadani.equals(user.getZadani())) || zadani.getStav() != ZadaniStatus.ACCEPTED);
    }

    @Override
    public boolean deleteSemestralkaDisabled(Zadani zadani, User user) {
        return (user.getZadani() == null || zadani == null || !(zadani.equals(user.getZadani())) || (user.getStudentsParalelka().getDeadlineVyber().getTime()) < (Calendar.getInstance().getTime().getTime()));


    }

    @Override
    public boolean odevzdatDisable(User user) {
        return (user.getZadani() == null);

    }
    //</editor-fold>

}

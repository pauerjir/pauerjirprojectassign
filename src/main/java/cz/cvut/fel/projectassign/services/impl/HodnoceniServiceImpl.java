package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.asf.notifications.template.NotificationSender;
import cz.cvut.fel.projectassign.daos.impl.HodnoceniDAOImpl;
import cz.cvut.fel.projectassign.model.Hodnoceni;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.services.HodnoceniService;
import cz.cvut.fel.projectassign.web.notifications.ProjectEvaluatedNotification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o hodnocení
 * @author Tom
 */
@Transactional
@Component("hodnoceniService")
public class HodnoceniServiceImpl implements HodnoceniService {

    @Autowired
    private HodnoceniDAOImpl dao;
    
    @Autowired
    private NotificationSender notificationSender;

    @Override
    public Hodnoceni create(String komentar, User user) {
        return dao.create(komentar, user);
    }

    @Override
    public Hodnoceni findById(int id) {
        return dao.findById(id);
    }

    @Override
    public void update(Hodnoceni hodnoceni) {
//        Hodnoceni h = dao.findById(hodnoceni.getId());
//        h.setHodnoceni(hodnoceni.getHodnoceni());
//        dao.save(h);
        notificationSender.send(new ProjectEvaluatedNotification(hodnoceni.getUser()));
    }
}
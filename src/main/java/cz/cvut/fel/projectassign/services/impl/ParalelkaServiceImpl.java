package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.daos.impl.ParalelkaDAOImpl;
import cz.cvut.fel.projectassign.model.Paralelka;
import cz.cvut.fel.projectassign.services.ParalelkaService;
import java.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará o paralelku
 *
 * @author Tom
 */
@Transactional
@Component("paralelkaService")
public class ParalelkaServiceImpl implements ParalelkaService {

    @Autowired
    private ParalelkaDAOImpl dao;

    @Override
    public Paralelka create(int cislo, int cas, int den) {
        return dao.create(cislo, cas, den);
    }

    @Override
    public Paralelka create(int cislo) {
        return dao.create(cislo);
    }

    @Override
    public Paralelka getByClassNumber(int i) {

        return dao.findByNumber(i);
    }

    @Override
    public void update(Paralelka par) {
        //Paralelka p = dao.findById(par.getId());
        //p.setDeadlineVyber(par.getDeadlineVyber());
        //dao.save(par);
    }

    @Override
    public List<Paralelka> getAllParalelka() {
        return dao.findAll();
    }

    @Override
    public Paralelka findById(int parseInt) {
        return dao.findById(parseInt);
    }
    
    @Override
    public Paralelka findByRequestedZadaniId(int zadaniId) {
        return dao.findByRequestedZadaniId(zadaniId);
    }

    @Override
    public String getDayString(Paralelka paralelka) {
        switch (paralelka.getDen()) {
            case 1:
                return "Pondělí";
            case 2:
                return "Úterý";
            case 3:
                return "Středa";
            case 4:
                return "Čtvrtek";
            case 5:
                return "Pátek";
            case 6:
                return "Sobota";
            case 7:
                return "Neděle";
            default:
                return "Chybný den";
        }
    }

    @Override
    public String getCasString(Paralelka paralelka) {
        switch (paralelka.getCas()) {
            case 1:
                return "7:30-9:00";
            case 3:
                return "9:15-10:45";
            case 5:
                return "11:00-12:30";
            case 7:
                return "12:45-14:15";
            case 9:
                return "14:30-16:00";
            case 11:
                return "16:15-17:45";
            case 13:
                return "18:00-19:30";
            case 15:
                return "19:45-pozde";
            default:
                return "Chybný čas";
        }
    }

    /**
     * nastaví seznam paralelek
     *
     * @return
     */
    @Override
    public SortedMap<String, String> setMapParalelky() {
        SortedMap<String, String> paralelky = new TreeMap<String, String>();

        List<Paralelka> allParalelky = getAllParalelka();
        Map<String, String> sortStrings = new HashMap<String, String>();
        Iterator<Paralelka> iter = allParalelky.iterator();
        while (iter.hasNext()) {
            Paralelka p = iter.next();
            sortStrings.put("" + p.getCisloParalelky(), "" + p.getCisloParalelky());
        }
        //Collections.sort(sortStrings);
        paralelky.putAll(sortStrings);
        paralelky.put("- Všechny paralelky:  ", "");
        return paralelky;
    }
}
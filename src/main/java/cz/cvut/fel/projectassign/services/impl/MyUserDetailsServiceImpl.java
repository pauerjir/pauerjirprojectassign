/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fel.projectassign.services.impl;

import cz.cvut.fel.projectassign.daos.impl.UserDAOImpl;
import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Třída reprezentující servisni (aplikační) vrstvu, která se stará autentifikaci a priřazení rolí
 * @author Tom
 */
@Transactional
@Component("customUserDetailsService")
public class MyUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAOImpl dao;
    
  
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException, DataAccessException {
        
        
        User user = dao.findByName(s);

        List<GrantedAuthority> AUTHORITIES = new ArrayList<GrantedAuthority>();

        if (user != null) {
            
            
            if(user.hasRole(UserRole.USER)) AUTHORITIES.add(new GrantedAuthorityImpl("ROLE_USER"));
            if(user.hasRole(UserRole.TUTOR)) AUTHORITIES.add(new GrantedAuthorityImpl("ROLE_TUTOR"));
            if(user.hasRole(UserRole.ADMIN)) AUTHORITIES.add(new GrantedAuthorityImpl("ROLE_ADMIN"));          
            org.springframework.security.core.userdetails.User springUser;
            springUser = new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(), true, true, true, true, AUTHORITIES);

            return springUser;

        } else {
            throw new UsernameNotFoundException("User not found:" + s);
        }

    }
}
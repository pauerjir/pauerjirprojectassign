package cz.cvut.fel.projectassign.services;

import cz.cvut.fel.projectassign.model.User;
import cz.cvut.fel.projectassign.model.UserRole;

/**
 * rozhrani pro servisni vrstvu zabyvajíci se uživatelskými rolemi
 * @author Tom
 */
public interface UserRoleService {

    public UserRole create(User user, int role);

    //public void update(UserRole ur);
}
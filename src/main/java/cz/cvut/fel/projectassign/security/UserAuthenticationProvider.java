package cz.cvut.fel.projectassign.security;

import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

/**
 *
 * @author balikm1
 */
public class UserAuthenticationProvider extends DaoAuthenticationProvider {
    
    private String mAuthorizationPath;
    private boolean mUseDbPasswd = false;

    //<editor-fold defaultstate="collapsed" desc="Properties">
    public String getAuthorizationPath() {
        return mAuthorizationPath;
    }

    public void setAuthorizationPath(String authorizationPath) {
        mAuthorizationPath = authorizationPath;
    }

    public boolean getUseDbPasswd() {
        return mUseDbPasswd;
    }

    public void setUseDbPasswd(boolean useDbPasswd) {
        mUseDbPasswd = useDbPasswd;
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Overriden methods">
    @Override
    protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
        if (mUseDbPasswd) {
            super.additionalAuthenticationChecks(userDetails, authentication);
            return;
        }

        if (authentication.getCredentials() == null) {
            throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }

        String presentedPassword = authentication.getCredentials().toString();

        if (!checkUserIdentity(userDetails.getUsername(), presentedPassword)) {
            throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"));
        }
    }
    //</editor-fold>
    
    //<editor-fold defaultstate="collapsed" desc="Private methods">
    private boolean checkUserIdentity(final String userName, final String password) {          
        try { // call authentization service
            Process extAuthProc = Runtime.getRuntime().exec(mAuthorizationPath);
            PrintWriter procInput = new PrintWriter(extAuthProc.getOutputStream());
            InputStreamReader procOut = new InputStreamReader(extAuthProc.getInputStream());
            InputStreamReader procErr = new InputStreamReader(extAuthProc.getErrorStream());
            procInput.println(userName);
            procInput.println(password);
            procInput.flush();
            procInput.close();
            procOut.read();
            procErr.read();
            procOut.close();
            procErr.close();

            if (extAuthProc.waitFor() == 0) {
                //login OK                                
                return true;
            } else {
                //auth failure                               
                return false;
            }
        } catch (Exception ex) {
            Logger.getLogger(UserAuthenticationProvider.class.getName()).log(Level.SEVERE, null, ex);
            throw new RuntimeException("Error while authenticating user", ex);
        }
    }
    //</editor-fold>
}
